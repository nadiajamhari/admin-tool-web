<?php

namespace App\Exports;
use DateTime;
use App\Models\TaxExemption;
use App\Models\Addresses;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithTitle;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Support\Facades\DB;
use \Maatwebsite\Excel\Sheet;

class TaxExemptionsExportExcel implements FromView, ShouldAutoSize
{
    private $IdPerSheets;

    public function __construct(Array $IdPerSheets)
    {
        $this->IdPerSheets = $IdPerSheets;
    }

    public function view(): View
    {       
        return view('exports.taxExemptionExcel', [
            'taxs' => TaxExemption::with('TaxExemptionsDonationReceipt','TaxExemptionsDonor')
            ->whereIn('tax_exemptions.id',$this->IdPerSheets)
            ->join('addresses', 'addresses.id', '=' , 'tax_exemptions.addressId')
            ->join('addresses_states', 'addresses_states.id', '=' , 'addresses.stateId')
            ->select('tax_exemptions.*',
            'addresses.address',
            'addresses.postalCode',
            'addresses.city',
            'addresses_states.state',)
            ->get()
        ]);
    }

}
