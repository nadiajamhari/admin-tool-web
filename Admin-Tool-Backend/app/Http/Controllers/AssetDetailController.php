<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AssetDetailController extends Controller
{
    public function index()
    {
        $details = AssetDetail::with('AssetCategory', 'AssetItem')->get();
        foreach ($details as $detail) {
            $department = AssetCategory::find($detail->category_id)->AssetDepartment;
            $detail['asset_department'] = $department;
        }
        return $details;
    }

    public function show($id)
    {
        $detail = AssetDetail::with('AssetCategory', 'AssetItem')->get()->find($id);
        $department = AssetCategory::find($detail->category_id)->AssetDepartment;
        $detail['asset_department'] = $department;
        return $detail;
    }

    public function update(Request $request, $id)
    {
        $asset = AssetDetail::find($id);

        if ($request->total_quantity != null) {
            $request->validate([
                'total_quantity' => ['required', 'numeric'],
            ]);

            /** if multiple_use, update quantity and create new qr code. */

            if ($asset->single_use == false) {

                /** check quantity */

                if ($request->total_quantity > $asset->total_quantity) {
                    $new_qty = $request->total_quantity - $asset->total_quantity;

                    $category = AssetCategory::where('id', $asset->category_id)->get();
                    $prefix = $category[0]->prefix;

                    $cate_count = AssetDetail::where('category_id', $asset->category_id)->max('category_count') + 1;
                    $pad_cate = str_pad($cate_count, 3, '0', STR_PAD_LEFT);

                    for ($i = 0; $i < $new_qty; $i++) {
                        $item_count = AssetItem::where('detail_id', $asset->id)->max('item_count') + 1;
                        $fullCode = 'CSM-' . $prefix . '-' . Carbon::now()->year . '-' . $pad_cate . '-' . $item_count;

                        $file_name = $fullCode . '.png';
                        $arr = array('full_code' => $fullCode);
                        $content = json_encode($arr);
                        $destinationPath = Storage::path('public/qrcodes/' . $file_name);
                        QrCode::format('png')
                            ->size(400)
                            ->generate($content, $destinationPath);
                        $file_path = env('APP_URL') . Storage::url('qrcodes/' . $file_name);
                        $data = ['file_name' => $file_name, 'file_path' => $file_path];
                        $newQrcode = AssetQrcode::create($data);
                        $qrcode_id = $newQrcode->id;

                        AssetItem::create([
                            'status_id' => 1,
                            'qrcode_id' => $qrcode_id,
                            'code' => $fullCode,
                            'detail_id' => $asset->id,
                            'item_count' => $item_count,
                            'year' => Carbon::now()->year,
                        ]);
                    }
                } else if ($request->total_quantity < $asset->total_quantity) {
                    return response("Please delete unwanted items first.", 400);
                // } else {
                //     return response("No changes", 200);
                }
            }
            $asset->update($request->all());
            return response()->json(['status' => 200, 'message' => 'Department updated successfully']);
        } else {
            $validator = Validator::make($request->all(), [
                'description' => 'required',
                'brand' => 'nullable',
                'unit_price' => 'nullable',
            ]);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->messages()]);
            } else {
                $asset->update($request->all());
                return response()->json(['status' => 200, 'message' => 'Department updated successfully']);
            }
        }
    }

    public function destroy($detail_id)
    {
        $assets = AssetItem::where('detail_id', $detail_id)->get();
        if (count($assets) != 0) {
            $src_arr = array();
            foreach ($assets as $asset) {
                $file_name = $asset->AssetQrcode->file_name;
                $path = 'public/qrcodes/' . $file_name;

                if (Storage::exists($path)) {
                    array_push($src_arr, $asset->AssetQrcode);
                }
            }
            AssetDetail::destroy($detail_id);
            if (count($src_arr) != 0) {
                foreach ($src_arr as $src) {
                    AssetQrcode::destroy($src->id);
                    Storage::delete('public/qrcodes/' . $src->file_name);
                }
            }
            return response('deleted', 200);
        } else {
            AssetDetail::destroy($detail_id);
            return response('deleted', 200);
        }
    }
}
