<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\AssetLog;
use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use Illuminate\Http\Request;
use App\Models\AssetCategory;
use App\Exports\AssetItemExport;
use App\Imports\AssetItemImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class AssetItemController extends Controller
{
    public function index()
    {
        return AssetItem::with('AssetQrcode', 'AssetStatus')->get();
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category_id' => 'required',
            'description' => 'required',
            'brand' => 'nullable',
            'unit_price' => 'nullable',
            'quantity' => ['required', 'numeric', 'min:0', 'not_in:0'],
            'single_use' => 'required|boolean',

            'year' => ['required', 'numeric', 'min:1900', 'max:' . date("Y")],
            'location' => 'nullable',
            'remark' => 'nullable',
            'status_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $category = AssetCategory::find($request->category_id);
            $prefix = $category->prefix;

            $cate_count = AssetDetail::where('category_id', $request->category_id)->max('category_count') + 1;
            $pad_cate = str_pad($cate_count, 3, '0', STR_PAD_LEFT);

            $details = AssetDetail::where('description', '=', $request->description)->first();

            if ($details === null) {
                // details does not exists
                $newDetail = AssetDetail::create([
                    'category_id' => $request->category_id,
                    'category_count' => $cate_count,
                    'unit_price' => $request->unit_price,
                    'total_quantity' => $request->quantity,
                    'single_use' => $request->single_use,
                    'description' => $request->description,
                    'brand' => $request->brand,
                    'min_quantity' => 0,
                ]);

                if ($request->single_use == false) {
                    for ($i = 0; $i < $request->quantity; $i++) {
                        $item_count = AssetItem::where('detail_id', $newDetail->id)->max('item_count') + 1;
                        $fullCode = 'CSM-' . $prefix . '-' . $request->year . '-' . $pad_cate . '-' . $item_count;

                        // generate QRCode
                        $file_name = $fullCode . '.png';
                        $arr = array('full_code' => $fullCode);
                        $content = json_encode($arr);
                        $destinationPath = Storage::path('public/qrcodes/' . $file_name);
                        QrCode::format('png')
                            ->size(400)
                            ->generate($content, $destinationPath);
                        $file_path = env('APP_URL') . Storage::url('qrcodes/' . $file_name);
                        $data = ['file_name' => $file_name, 'file_path' => $file_path];
                        $newQrcode = AssetQrcode::create($data);
                        $qrcode_id = $newQrcode->id;

                        AssetItem::create([
                            'status_id' => $request->status_id,
                            'qrcode_id' => $qrcode_id,
                            'code' => $fullCode,
                            'detail_id' => $newDetail->id,
                            'item_count' => $item_count,
                            'year' => $request->year,
                            'location' => $request->location,
                            'remark' => $request->remark,
                        ]);
                    }
                } else {
                    $item_count = 00;
                    $fullCode = 'CSM-' . $prefix . '-' . $request->year . '-' . $pad_cate . '-' . $item_count;

                    // generate QRCode
                    $file_name = $fullCode . '.png';
                    $arr = array('full_code' => $fullCode);
                    $content = json_encode($arr);
                    $destinationPath = Storage::path('public/qrcodes/' . $file_name);
                    QrCode::format('png')
                        ->size(400)
                        ->generate($content, $destinationPath);
                    $file_path = env('APP_URL') . Storage::url('qrcodes/' . $file_name);
                    $data = ['file_name' => $file_name, 'file_path' => $file_path];
                    $newQrcode = AssetQrcode::create($data);
                    $qrcode_id = $newQrcode->id;

                    AssetItem::create([
                        'status_id' => $request->status_id,
                        'qrcode_id' => $qrcode_id,
                        'code' => $fullCode,
                        'detail_id' => $newDetail->id,
                        'item_count' => $item_count,
                        'year' => $request->year,
                        'location' => $request->location,
                        'remark' => $request->remark,
                    ]);
                }
                return response()->json(['status' => 200, 'message' => 'Asset created succesfully!']);
            } else {
                return response()->json(['status' => 409, 'errors' => 'Asset existed.']);
            }
        }
    }

    public function show($code)
    {
        return AssetItem::where('code', $code)->first();
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'status_id' => 'required',
            'location' => 'nullable',
            'remark' => 'nullable',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else {
            $asset = AssetItem::find($id);
            $asset->update($request->all());
            return response()->json(['status' => 200, 'message' => 'Asset item updated successfully']);
        }
    }

    public function destroy($asset_id)
    {
        $asset = AssetItem::with('AssetQrcode', 'AssetDetail')->get()->find($asset_id);
        $detail_id = $asset->AssetDetail->id;
        $qr_id = $asset->AssetQrcode->id;
        $file_name = $asset->AssetQrcode->file_name;
        $src = 'public/qrcodes/' . $file_name;

        if (Storage::exists($src)) {
            AssetItem::destroy($asset_id);
            AssetQrcode::destroy($qr_id);
            Storage::delete($src);
            AssetDetail::find($detail_id)->update(array('total_quantity' => $asset->AssetDetail->total_quantity - 1));
            return response('deleted', 200);
        } else {
            return response('failed', 404);
        }
    }

    public function exportExcelTemplate()
    {
        return Excel::download(new AssetItemExport, 'asset-template.xlsx');
    }

    public function importFromExcel(Request $request)
    {
        // $this->validate($request, ['file' => 'required']); //|mimes:xls, xlsx

        $validator = Validator::make($request->all(), ['file' => 'required']);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        } else if ($request->file->getClientOriginalExtension() != "xlsx") {
            return response()->json(['errors' => ['file' => ['File extension must be .xlsx']]]);
        } else {
            $file = $request->file;
            // echo $file->getClientOriginalName();
            // echo $file->getClientOriginalExtension();

            // echo  $file->getRealPath();

            $file->storeAs('public/temp', $file->getClientOriginalName());
            // $filePath = '/storage/temp/' . $file->getClientOriginalName();

            //  return ;

            // echo $filePath;

            // $asset = asset($filePath);

            // // echo $asset;
            try {
                $filePath = Storage::path('/public/temp/' . $file->getClientOriginalName());
                $import = new AssetItemImport;
                $import->import($filePath);
                return response()->json(['status' => 200, 'message' => 'Assets are imported succesfully']);
            } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
                $master_error_array = [];
                $failures = $e->failures();
                foreach ($failures as $failure) {
                    $single_error_array = [];
                    // $single_error_array[$failure->row()] = $failure->errors();
                    $single_error_array['row'] = $failure->row(); // row that went wrong
                    // $failure->attribute(); // either heading key (if using heading row concern) or column index
                    $single_error_array['errors'] = $failure->errors(); // Actual error messages from Laravel validator
                    // $failure->values(); // The values of the row that has failed.
                    array_push($master_error_array, $single_error_array);
                }
                return response()->json(['errors' => $master_error_array]);
            }
        }
    }

    // TAK DIGUNAKAN
    public function updateStatus()
    {
        $assets = AssetItem::all();

        foreach ($assets as $asset => $value) {
            if ($value->quantity < $value->min_quantity) {
                AssetItem::where('id', $value->id)->update(array('status_id' => '2'));
            } elseif ($value->quantity === 0) {
                AssetItem::where('id', $value->id)->update(array('status_id' => '3'));
            } else {
                AssetItem::where('id', $value->id)->update(array('status_id' => '1'));
            }
        }
    }

    public function itemsByDetail($id)
    {
        $items = AssetItem::where('detail_id', $id)->get();
        $detail = AssetDetail::find($id);
        foreach ($items as $item) {
            $asset_log = array();
            foreach ($item->AssetLog as $log) {
                $staff = User::find($log->staff_id);
                $formatted = [
                    'staff_name' => $staff->name,
                    'item_code' => $item->code,
                    'single_use' => $detail->single_use,
                    'purpose' => $log->purpose,
                    'requested_qty' => $log->requested_qty,
                    'returned_qty' => $log->returned_qty,
                    'requested_at' => $log->requested_at,
                    'returned_at' => $log->returned_at,
                ];
                array_push($asset_log, $formatted);
            }
            $item->asset_log_formatted  = $asset_log;
        }
        return $items;
    }
}
