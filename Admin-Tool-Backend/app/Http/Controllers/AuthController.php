<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mails;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Mail;
use App\Mail\ForgotPassword;
use App\Models\UsersRole;

class AuthController extends Controller
{
    public function register(Request $request)
    {

        //assuming that this is registration for first Admin CSM (because using API)
        $fields = $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::create([
            'name' => $fields['name'],
            'email' => $fields['email'],
            'password' => bcrypt($fields['password']),
            'roleId' => 1, //Role id always be one for the first time register
        ]);

        $token = $user->createToken('myapptoken')->plainTextToken;

        $response = [
            'user' => $user,
            'token' => $token
        ];

        return response($response, 201);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();
        return [
            'message' => 'Logged out'
        ];
    }

    public function login(Request $request)
    {
        $fields = $request->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);

        $errors = [];

        // Check email
        $user = User::where('email', $fields['email'])->first();

        // Check password
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            if (!$user) {
                $errors['email'] = 'Incorrect email';
            } else if (!Hash::check($fields['password'], $user->password)) {
                $errors['password'] = 'Incorrect password';
            }
            return response()->json(['errors' => $errors]);
        } else {
            $token = $user->createToken('myapptoken')->plainTextToken;

            $currentUser = [
                'name' =>   $user->name,
                'email' => $user->email,
                'role' => UsersRole::where('id', $user->roleId)->first()->role
            ];

            $response = [
                'user' => $currentUser,
                'token' => $token,
                'status' => 200
            ];

            return response()->json($response);
        }
    }

    public function forgotPasswordEmail(Request $request)
    {

        $fields = $request->validate([
            'email' => 'required|email',
            'urlPassword' => 'required',
        ]);

        if (!$user = User::where('email', $fields['email'])->first()) {
            return response()->json(['error' => 'User doesn\'t exists!'], 401);
        }

        $token = Str::random(60);

        DB::table('password_resets')->insert(
            ['email' => $fields['email'], 'token' => $token, 'created_at' => Carbon::now()]
        );

        $url = $fields['urlPassword'] . $token;


        $details = [
            'name' => $user->name,
            'token' => $url
        ];

        Mail::to($request->email)->send(new ForgotPassword($details));

        // Mail::send('email.forgotPassword', ['token' => $url , 'name' => $user->name], function($message) use ($request , $fields) {
        //           $message->from($request->email);
        //           $message->to($fields['email']);
        //           $message->subject('Reset Password Notification');
        //        });
        $response = [
            'token' => $token,
            'message' => 'We have e-mailed your password reset link!',
        ];

        return response($response, 201);
    }

    public function updatePasswordByToken(Request $request)
    {

        $fields = $request->validate([
            'token' => 'required',
            'password' => 'required|string'
        ]);

        $query = DB::table('password_resets')->where('token', $fields['token']);
        $passwordResets = $query->first();

        if (!$passwordResets) {

            return response()->json(['error' => 'Invalid Token'], 401);
        }

        /** @var User $user */

        if (!$user = User::where('email', $passwordResets->email)->first()) {
            return response()->json(['error' => 'User doesn\'t exists!'], 401);
        }

        $user->password = bcrypt($fields['password']);
        $user->save();

        $query->delete();
        $response = [
            'message' => 'Successfuly change your password. You will redirect to login page in few seconds',
        ];

        return response($response, 201);
    }

    public function changePassword(Request $request)
    {

        $fields = $request->validate([
            'currentPassword' => 'required',
            'newPassword' => 'required',
            'confirmPassword' => 'required|same:newPassword',
        ]);

        $currentUser = auth()->user();

        if (!Hash::check($fields['currentPassword'], $currentUser->password)) {
            return response([
                'message' => 'Your current password is invalid'
            ], 401);
        }

        $currentUser->password = bcrypt($fields['newPassword']);
        $currentUser->save();

        return $currentUser;
    }

    public function checkToken(Request $request)
    {
        $token = $request->bearerToken();
        // return $token;
        if ($token === "null") {
            return response()->json(['error' => 'No token!']);
        } else {
            [$id, $user_token] = explode('|', $token, 2);
            $token_data = DB::table('personal_access_tokens')->where('token', hash('sha256', $user_token))->first();
            if (!$token_data) {
                return response()->json(['error' => 'Invalid token!', 'status' => 401]);
            } else {
                return response()->json(['message' => [$id, $user_token, $token_data], 'status' => 200]);
            }
        }
    }
}
