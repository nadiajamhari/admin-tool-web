<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DonorAnalysisNewDonation;
use App\Models\DonorAnalysisActiveDonor;
use App\Models\DonorAnalysisNotActiveDonor;
use App\Models\DonorAnalysisRfmPoint;
use App\Models\DonorAnalysisMonthBestDonor;
use App\Models\DonorAnalysisMonthHighMDonor;
use App\Models\DonorAnalysisMonthLowMDonor;
use App\Models\DonorAnalysisMonthOldDonor;
use App\Models\DonorAnalysisMonthState;
use App\Models\TaxExemption;
use App\Models\TaxExemptionsDonationReceipt;
use App\Models\TaxExemptionsDonor;
use App\Models\AddressesState;
use App\Models\StatusColor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Mail;
use Carbon\Carbon;

class DonorAnalysisController extends Controller
{

    public function index(){
        $bestDonorData = DonorAnalysisMonthBestDonor::with('DonorAnalysisActiveDonor')->get();
        $highMDonorData= DonorAnalysisMonthHighMDonor::with('DonorAnalysisActiveDonor')->get();
        $lowMDonorData = DonorAnalysisMonthLowMDonor::with('DonorAnalysisActiveDonor')->get();
        $oldDonorData = DonorAnalysisMonthOldDonor::with('DonorAnalysisActiveDonor')->get();

        //declaration null
        $topThreeBestDonor=null;
        $topThreeHighMDonor=null;
        $topThreelowMDonor=null;
        if(count($oldDonorData) <=0){
            $oldDonorData=null;
        }
        
        if(count($bestDonorData)>=3){
            for($i = 0 ; $i<3;$i++){
                //search name,phonenumber, and email [basic details]
                $topThreeBestDonor[$i]=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$bestDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
                $topThreeBestDonor[$i]["frequently"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
                $topThreeBestDonor[$i]["recently"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
                $topThreeBestDonor[$i]["monetary"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
                $topThreeBestDonor[$i]["totalMark"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
            }
        }

        if(count($highMDonorData)>=3){
            for($i = 0 ; $i<3;$i++){
                //search name,phonenumber, and email [basic details]
                $topThreeHighMDonor[$i]=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$highMDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
                $topThreeHighMDonor[$i]["frequently"]=$highMDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
                $topThreeHighMDonor[$i]["recently"]=$highMDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
                $topThreeHighMDonor[$i]["monetary"]=$highMDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
                $topThreeHighMDonor[$i]["totalMark"]=$highMDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
            }
        }

        if(count($lowMDonorData)>=3){
            for($i = 0 ; $i<3;$i++){
                //search name,phonenumber, and email [basic details]
                $topThreelowMDonor[$i]=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$lowMDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
                $topThreelowMDonor[$i]["frequently"]=$lowMDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
                $topThreelowMDonor[$i]["recently"]=$lowMDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
                $topThreelowMDonor[$i]["monetary"]=$lowMDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
                $topThreelowMDonor[$i]["totalMark"]=$lowMDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
            }
        }
       
        // for($i = 0 ; $i<3;$i++){
        //     //search name,phonenumber, and email [basic details]
        //     $oldDonor[$i]=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$oldDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
        //     $oldDonor[$i]["frequently"]=$oldDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
        //     $oldDonor[$i]["recently"]=$oldDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
        //     $oldDonor[$i]["monetary"]=$oldDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
        //     $oldDonor[$i]["totalMark"]=$oldDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
        // }
    
        return $response = ([
            'topThreeBestDonor'=>$topThreeBestDonor,
            'topThreeHighMDonor'=>$topThreeHighMDonor,
            'topThreelowMDonor'=>$topThreelowMDonor,
            'oldDonor' => $oldDonorData,
        ]);

    }
    public function donorAnalysisAlgorithm(){

        $donorData = DonorAnalysisNewDonation::all();

        //algorithm for declaring mark
        if(count($donorData)>0) {
            
        $result = DB::transaction (function() use ($donorData) { // declaring newDatas to check status true/false either the data is new for donorAnalysisActiveDonors Table or not
            // declaring donors to store a data that should be compare in comparison algorithm to store in donorAnalysisActiveDonors.
            // frequently to calculate the total of frequently (number of receipt) for each tax exemptions
            // monetary to calculate the total amount that donors have been donated.
            $newDatas = [];
            $donors=[];
            $donorsNew=[];
            $frequently=0;
            $monetary=0.00;
        
            //convert an object to array, so that can use array_splice when find a same donorId in donorAnalysisNewDonations table
            $arrayOfDonorData =json_decode(json_encode($donorData), true);

            // //algorithm for comparison and store data into donorAnalysisActiveDonor -begin

            // //use binary comparison and compare.
            //get all RFM point in DB (Reference point to compare)
            $rfmPoint = DonorAnalysisRfmPoint::all();
            // mid = left + (right-left)/2;
            $currentMid= 0 + (count($rfmPoint)-1-0)/2;

            //algorithm newDonor into donorActive DB
            for($i = 0 ; $i < count($arrayOfDonorData) ; $i++) {
                $created_at = $arrayOfDonorData[$i]['created_at'];
                $new_created_at= Carbon::parse($created_at)->format('Y-m-d H:i:s');
                $frequently += TaxExemptionsDonationReceipt::where('taxId',$arrayOfDonorData[$i]['taxId'])->count();
                $monetary += TaxExemption::where('id',$arrayOfDonorData[$i]['taxId'])->value('amount');

                //store monetary at variable before finalize
                $monetary = $monetary;

                $donorId = $arrayOfDonorData[$i]['donorId'];
                $recently=(Carbon::now()->diffInDays($arrayOfDonorData[$i]['created_at']))+1;

                //if the data is exsiting in donorAnalysisActiveDonor, it should take the old frequently 
                // and add with the new frequently [same as monetary]
                if($data = donorAnalysisActiveDonor::where('donorId', $donorId)->first()) {
                    $frequently += $data->frequently;
                    $monetary += ($data->monetary)*$data->frequently;
                    $newData= false;
                }

                else if($data = donorAnalysisNotActiveDonor::where('donorId', $donorId)->first()) {
                    $newData= true;
                    //remove the data from donor_analysis_not_active_donors table
                    donorAnalysisNotActiveDonor::destroy($data->id);
                }

                else {
                    $newData= true;
                }
                
                //if having a donor that apply tax in same day.
                if($count=DonorAnalysisNewDonation::where('donorId',$donorId)->count() > 1){
                    // to loop donor data go get the same id and calculate together 
                    for($j = $i+1; $j < count($arrayOfDonorData) ; $j++) {
                        if($arrayOfDonorData[$j]['donorId']==$donorId){
                            $frequently += TaxExemptionsDonationReceipt::where('taxId',$arrayOfDonorData[$j]['taxId'])->count();
                            $monetary += TaxExemption::where('id',$arrayOfDonorData[$j]['taxId'])->value('amount');
                            array_splice($arrayOfDonorData, $j,1);
                           break;
                        }
                        //another if to break when the loop already looping @$count-1;
                    }
                }

                //store the actual monetary [monetary should be an average total Monetary/frequently]
                $monetary = round(($monetary/$frequently), 2);

                //compare frequently
                $frequentlyPoint = $this->binaryCompare($frequently, "frequently");
                //compare monetary
                $monetaryPoint = $this->binaryCompare($monetary, "monetary");
                //compare recently
                $recentlyPoint = $this->binaryCompare($recently, "recently");

                //active group always be 1 if create/update data;
                if($newData==true){
                    $donorsNew[$i]["message1"] = "okay new data akan simpan";
                    donorAnalysisActiveDonor::create([
                        "donorId" =>$donorId,
                        "totalDaysFromLastUpdated" => $recently,
                        "dateLastUpdateAsDonation" => $new_created_at,
                        "frequently" => $frequently,
                        "monetary" =>$monetary,
                        "recentlyPoint" => $recentlyPoint,
                        "frequentlyPoint" =>$frequentlyPoint,
                        "monetaryPoint"=>$monetaryPoint,
                        "activeGroup"=>1,
                        "totalMark"=>$recentlyPoint+$monetaryPoint+$frequentlyPoint,
                        "created_at"=>Carbon::now(),
                    ]);
                }

                else {
                    $existDonor = donorAnalysisActiveDonor::where('donorId',$donorId)->first();
                    $existDonor->totalDaysFromLastUpdated  = $recently;
                    $existDonor->dateLastUpdateAsDonation  =$new_created_at;
                    $existDonor->frequently = $frequently;
                    $existDonor->monetary = $monetary;
                    $existDonor->recentlyPoint = $recentlyPoint;
                    $existDonor->frequentlyPoint = $frequentlyPoint;
                    $existDonor->monetaryPoint = $monetaryPoint;
                    $existDonor->activeGroup = 1;
                    $existDonor->totalMark = $recentlyPoint+$monetaryPoint+$frequentlyPoint;
                    $existDonor->save();
                }
                //set the FM to the default data as it going to calculate the next iteration
                $frequently = 0;
                $monetary=0.00;
            }

            //reCheck Recently Again
            $this->reCheckRecently();
            
            //Sorting algorithm
           $activeDonor_highRecently = donorAnalysisActiveDonor::where('recentlyPoint', 100)->get();
           $arrayOf_highRecently =json_decode(json_encode($activeDonor_highRecently), true);
           $activeDonor_lowRecently = donorAnalysisActiveDonor::where('recentlyPoint','<=', 40)->get();

           //sorting bestDonor algorithm
           $bestDonor = donorAnalysisActiveDonor::where('recentlyPoint',100)->where('frequentlyPoint',100)->where('monetaryPoint',100)->orderBy('updated_at', 'desc')->get();

           //delete all data as its going to sort new ranking
           $deletedBestDonor = DB::delete('delete from donor_analysis_month_best_donors');
           //save at donor_analysis_month_best_donors DB
           if(count($bestDonor) >0 ){
               forEach($bestDonor as $donor) {
                   DonorAnalysisMonthBestDonor::create([
                       'activeDonorId' => $donor->id,
                        'created_at'=>Carbon::now(),
                    ]);
                }
                 //remove data $bestDonor from $activeDonor_highRecently
                //change best donor to array.
                $arrayOfBestDonor =json_decode(json_encode($bestDonor), true);
                $arrayOf_highRecently = $this->removeDonorFromArray($arrayOf_highRecently,$arrayOfBestDonor);
            }

            $checkDonorBestAll = DonorAnalysisMonthBestDonor::all();

           if(count($arrayOf_highRecently) >0 ) {
               //sorting newDonor_HighMonetary algorithm
           $newDonor_HightMonetary=[];
           $countNewDonor_HM = 0;

           for($i=0;$i<count($arrayOf_highRecently);$i++){
               if($arrayOf_highRecently[$i]["recentlyPoint"] == 100 && $arrayOf_highRecently[$i]["monetaryPoint"]==100){
                    $newDonor_HightMonetary[$countNewDonor_HM]=$arrayOf_highRecently[$i];
                    $countNewDonor_HM ++;
                    if(count($newDonor_HightMonetary)>1){
                        $newDonor_HightMonetary = $this->bubbleSort($newDonor_HightMonetary, $arrayOf_highRecently[$i]["frequentlyPoint"], "frequentlyPoint");
                    }
               }
            }

            //store newDonor_HightMonetary at donor_analysis_month_highM_donors DB
            $deletedNewDonor_HightMonetary = DB::delete('delete from donor_analysis_month_highm_donors');

            if(count($newDonor_HightMonetary) > 0){
                for($i=0; $i<count($newDonor_HightMonetary); $i++){
                    DonorAnalysisMonthHighMDonor::create([
                        'activeDonorId' => $newDonor_HightMonetary[$i]['id'],
                        'created_at'=>Carbon::now(),
                    ]);
                }
                //remove $newDonor_HighMonetary from arrayOfActive_highRecently
                $arrayOf_highRecently = $this->removeDonorFromArray($arrayOf_highRecently,$newDonor_HightMonetary);
            }
          
            $checkDonorHighM = DonorAnalysisMonthHighMDonor::all();

     

            //sorting newDonor_lowMonetary algorithm
            $newDonor_lowMonetary = [];
            $countNewDonor_LM  = 0;

            for($i=0;$i<count($arrayOf_highRecently);$i++){
                if($arrayOf_highRecently[$i]["recentlyPoint"] == 100 && $arrayOf_highRecently[$i]["frequentlyPoint"]==100){
                    $newDonor_lowMonetary[$countNewDonor_LM]=$arrayOf_highRecently[$i];
                    $countNewDonor_LM++;
                     if(count($newDonor_lowMonetary)>1){
                         $newDonor_lowMonetary = $this->bubbleSort($newDonor_lowMonetary, $arrayOf_highRecently[$i]["monetaryPoint"], "monetaryPoint");
                     }
                }
            }

            //store newDonor_HighMonetary at donor_analysis_month_lowM_donors DB
            $deletedNewDonor_LowMonetary = DB::delete('delete from donor_analysis_month_lowm_donors');

            if(count($newDonor_lowMonetary)>0) {
                for($i=0; $i<count($newDonor_lowMonetary); $i++){
                    DonorAnalysisMonthLowMDonor::create([
                        'activeDonorId' => $newDonor_lowMonetary[$i]['id'],
                        'created_at'=>Carbon::now(),
                    ]);
                }

                //remove $newDonor_lowMonetary from arrayOfActiveDonorG1
                $arrayOf_highRecently = $this->removeDonorFromArray($arrayOf_highRecently,$newDonor_lowMonetary);
            }
           

            $checkDonorLowM = DonorAnalysisMonthLowMDonor::all();

           }
           

            //algorithm On oldDonor [$activeDonor_lowRecently]
            $arrayOf_lowRecently =json_decode(json_encode($activeDonor_lowRecently), true);

            $oldDonors = [];
            $countOldDonors = 0;
            if(count($arrayOf_lowRecently)>0 ){
                for($i=0;$i<count($arrayOf_lowRecently);$i++){
                    if(($arrayOf_lowRecently[$i]["monetaryPoint"] == 100 || $arrayOf_lowRecently[$i]["monetaryPoint"] == 80) && ($arrayOf_lowRecently[$i]["frequentlyPoint"]==100 || $arrayOf_lowRecently[$i]["frequentlyPoint"]== 80)){
                        $oldDonors[$countOldDonors]=$arrayOf_lowRecently[$i];
                        $countOldDonors++;
                         if(count($oldDonors)>1){
                            $oldDonors = $this->bubbleSortDate($oldDonors, $arrayOf_lowRecently[$i]["updated_at"]);
                         }
                    }
                }
    
                //store oldDonors at donor_analysis_month_old_donors DB
                $deletedOldDonors = DB::delete('delete from donor_analysis_month_old_donors');
    
                if(count($oldDonors)>0) {
                    for($i=0; $i<count($oldDonors); $i++){
                        DonorAnalysisMonthOldDonor::create([
                            'activeDonorId' => $oldDonors[$i]['id'],
                            'created_at'=>Carbon::now(),
                        ]);
                    }
                }
            }
            
           
            $checkDonorOld = DonorAnalysisMonthOldDonor::all();
            $deletedNewDonation = DB::delete('delete from donor_analysis_new_donations');

            
            return $response = ["message" => "succesffully count"
            ];
        
        });

            return $result;
           
        }

        return $donorData;
    }

    public function bubbleSort( $arr , $numberCompare , $dataCompare) {
        $n = count($arr);
        for ($i=$n-1; $i >0; $i--){

            if($arr[$i][$dataCompare] !=$numberCompare){
                break;
            }
            if ($arr[$i][$dataCompare] > $arr[$i-1][$dataCompare])
            {
            // swap arr[i-1] and arr[i]
            $temp = $arr[$i];
            $arr[$i] = $arr[$i-1];
            $arr[$i-1] = $temp;
            }
                    
        }

       return $arr;
    }

    public function bubbleSortDate($arr , $dateCompare) {

        $dateCompare = Carbon::parse($dateCompare)->format('Y-m-d H:i:s');

        $n = count($arr);

        for ($i=$n-1; $i >0; $i--){

            //changeDate to format that can be compare
            $currentDate = Carbon::parse($arr[$i]["updated_at"])->format('Y-m-d H:i:s');
            $previousDate = Carbon::parse($arr[$i-1]["updated_at"])->format('Y-m-d H:i:s');

            if($currentDate!=$dateCompare){
                break;
            }
            if ($currentDate > $previousDate)
            {
            // swap arr[i-1] and arr[i]
            $temp = $arr[$i];
            $arr[$i] = $arr[$i-1];
            $arr[$i-1] = $temp;
            }
                    
        }

       return $arr;
    }

    public function removeDonorFromArray($arr , $itemsToRemove){

        for($i= 0; $i<count($itemsToRemove); $i++) {
            for($j=0; $j<count($arr); $j++){
                if($itemsToRemove[$i]['id']== $arr[$j]['id']){
                 array_splice($arr, $j,1);
                 break;
                }
            }
        }

        return $arr;
    }

    public function binaryCompare($itemToCompare, $nameOfComparison){

        // //use binary comparison and compare.
        //get all RFM point in DB (Reference point to compare)
        $rfmPoint = DonorAnalysisRfmPoint::all();
        // mid = left + (right-left)/2;
        $currentMid= 0 + (count($rfmPoint)-1-0)/2;

        if(Str::contains($nameOfComparison, 'frequently')){
            if($itemToCompare==$rfmPoint[$currentMid]->minimumFrequently){
                $frequentlyPoint = $rfmPoint[$currentMid]->mark;
            }
    
            //if not same as mid (at the right (because data has been sort descending))
            else if($itemToCompare>$rfmPoint[$currentMid]->minimumFrequently){
                for($j = 0; $j <=$currentMid;$j++){
                    if($itemToCompare>=$rfmPoint[$j]->minimumFrequently){
                        $frequentlyPoint = $rfmPoint[$j]->mark;
                        break;
                    }
                }
            }
            else{
                for($j = $currentMid; $j < count($rfmPoint);$j++){
                        if($itemToCompare>=$rfmPoint[$j]->minimumFrequently){
                            $frequentlyPoint = $rfmPoint[$j]->mark;
                            break;
                        }
                    }
            }

            return $frequentlyPoint;
        }

        else if(Str::contains($nameOfComparison, 'monetary')) {
            //compare monetary
                if($itemToCompare==$rfmPoint[$currentMid]->minimumMonetary){
                $monetaryPoint = $rfmPoint[$currentMid]->mark;
            }

            //if not same as mid (at the right (because data has been sort descending))
            else if($itemToCompare>$rfmPoint[$currentMid]->minimumMonetary){
                for($j = 0; $j <=$currentMid;$j++){
                    if($itemToCompare>=$rfmPoint[$j]->minimumMonetary){
                        $monetaryPoint = $rfmPoint[$j]->mark;
                        break;
                    }
                }
            }
            else{
                for($j = $currentMid; $j < count($rfmPoint);$j++){
                        if($itemToCompare>=$rfmPoint[$j]->minimumMonetary){
                            $monetaryPoint = $rfmPoint[$j]->mark;
                            break;
                        }
                    }
            }

            return $monetaryPoint;
        }

        else {
            //compare recently
            //change day into month
            $calculateMonth = round($itemToCompare/30);
            if((int)$calculateMonth < 1){
                $recentlyPoint = 100;
            }

            else {
                $month = (int)$calculateMonth;
                if($month==$rfmPoint[$currentMid]->minimumMonth){
                    $recentlyPoint = $rfmPoint[$currentMid]->mark;
                }

                //if not same as mid (at the right (because data has been sort ascending))
                else if($month>$rfmPoint[$currentMid]->minimumMonth){
                    for($j = count($rfmPoint) - 1; $j >=$currentMid;$j--){
                        if($month>=$rfmPoint[$j]->minimumMonth){
                            $recentlyPoint = $rfmPoint[$j]->mark;
                            break;
                        }
                    }
                }
                else{
                    for($j = $currentMid; $j >= 0 ;$j--){
                            if($month>=$rfmPoint[$j]->minimumMonth){
                                $recentlyPoint = $rfmPoint[$j]->mark;
                                break;
                            }
                        }
                }
            }

            return $recentlyPoint;
        }
    }

    public function reCheckRecently(){
        $activeDonor = DonorAnalysisActiveDonor::all();

        $todayDate = Carbon::now();

        forEach($activeDonor as $data){
           $dbDate = $data->updated_at;
           echo($dbDate);
           if($dbDate->ne($todayDate)){
               $lastDate=Carbon::parse($data->dateLastUpdateAsDonation);
               $newRecently = (Carbon::now()->diffInDays($lastDate))+1;
               $newRecentlyPoint = $this->binaryCompare($newRecently, "recently");
               $data->totalDaysFromLastUpdated=$newRecently;
               $data->recentlyPoint=$newRecentlyPoint;
               $data->save();
           }
        }
        return $todayDate;

    }

    public function topTenDonor($type){
        if($contains = Str::contains($type, 'best')){
            $donorData= DonorAnalysisMonthBestDonor::with('DonorAnalysisActiveDonor')->get();
        }

        else if($contains = Str::contains($type, 'high')){
            $donorData = DonorAnalysisMonthHighMDonor::with('DonorAnalysisActiveDonor')->get();
        }

        else if($contains = Str::contains($type, 'low')){
            $donorData = DonorAnalysisMonthLowMDonor::with('DonorAnalysisActiveDonor')->get();
        }

        else if($contains = Str::contains($type, 'old')){
            $donorData = DonorAnalysisMonthOldDonor::with('DonorAnalysisActiveDonor')->get();
        }
        
        for($i = 0 ; $i<count($donorData);$i++){
            //search name,phonenumber, and email [basic details]
            $donorTaxData=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$donorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
            $donorList[$i]["selected"] = false;
            $donorList[$i]["Id"] = $donorTaxData->TaxExemption->id;
            $donorList[$i]["Position"]=$i+1;
            $donorList[$i]["Name"]=$donorTaxData->TaxExemption->name;
            $donorList[$i]["Email"]=$donorTaxData->TaxExemption->email;
            $donorList[$i]["Frequently"]=$donorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
            $donorList[$i]["Recently"]=$donorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
            $donorList[$i]["Monetary"]=$donorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
            $donorList[$i]["AverageMonetary"]=$donorData[$i]->DonorAnalysisActiveDonor->monetary;
            $donorList[$i]["TotalMarks"]=$donorData[$i]->DonorAnalysisActiveDonor->totalMark;
        }

        return $response =[
            'donorList' => $donorList,
        ];
    }

    // public function donorState(){
    //     $data  = DonorAnalysisMonthState::with("AddressesState")->get();
    //     $taxs = TaxExemption::WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])->get();

    //     $totalMoney=0;

    //     forEach($tax)

    //     for($i=0; $i<count($data);$i++){
    //         $label[$i]=  $data[$i]->AddressesState->code;
    //         $quantity[$i]=  $data[$i]->quantity;
    //     }

    //     return $response = [
    //         "labels" => $label,
    //         "quantity" => $quantity,
    //         "totalTax" => count($taxs)
    //     ];
    // }

    public function donorStateByDate($startDate,$endDate){

        $states=AddressesState::all();
        $totalTax = TaxExemption::WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])->count();

        $totalMoneyByState = 0;
        $totalMoneyOverall = 0;
        for($i=0; $i<count($states);$i++){
            $label[$i]=  $states[$i]->code;
            $quantity[$i]= TaxExemption::join('addresses', 'addresses.id', '=' , 'tax_exemptions.addressId')
            ->WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])
            ->where("addresses.stateId", $states[$i]->id)
            ->count();

            $Money =TaxExemption::join('addresses', 'addresses.id', '=' , 'tax_exemptions.addressId')
            ->WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])
            ->where("addresses.stateId", $states[$i]->id)
            ->select("tax_exemptions.amount")->get();

            for($j=0;$j<count($Money);$j++){
                $totalMoneyByState +=$Money[$j]->amount;
                $totalMoneyOverall +=$Money[$j]->amount;
            }

            $stateMoney[$i]=$totalMoneyByState;
            $totalMoneyByState=0;
        }

        return $response = [
            "labels" => $label,
            "quantity" => $quantity,
            "totalMoneyByState" => $stateMoney,
            "totalMoney" => $totalMoneyOverall,
            "totalTax" => $totalTax,
        ];
    }

    // public function AllLastMonth(){
    //     //fetch all type donor for last month
    //     $bestDonorData= DonorAnalysiLMonthBestDonor::with('DonorAnalysisActiveDonor')->get();
    //     $HighDonorData = DonorAnalysisMonthHighMDonor::with('DonorAnalysisActiveDonor')->get();
    //     $LowDonorData = DonorAnalysisMonthLowMDonor::with('DonorAnalysisActiveDonor')->get();
        
    //     for($i = 0 ; $i<count($bestDonorData);$i++){
    //         //search name,phonenumber, and email [basic details]
    //         $donorTaxData=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$bestDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
    //         $BestDonorList[$i]["Id"] = $donorTaxData->TaxExemption->id;
    //         $BestDonorList[$i]["Position"]=$i+1;
    //         $BestDonorList[$i]["Name"]=$donorTaxData->TaxExemption->name;
    //         $BestDonorList[$i]["Email"]=$donorTaxData->TaxExemption->email;
    //         $BestDonorList[$i]["Frequently"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
    //         $BestDonorList[$i]["Recently"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
    //         $BestDonorList[$i]["Monetary"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
    //         $BestDonorList[$i]["TotalMarks"]=$bestDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
    //     }

    //     for($i = 0 ; $i<count($HighDonorData);$i++){
    //         //search name,phonenumber, and email [basic details]
    //         $donorTaxData=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$HighDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
    //         $highDonorList[$i]["Id"] = $donorTaxData->TaxExemption->id;
    //         $highDonorList[$i]["Position"]=$i+1;
    //         $highDonorList[$i]["Name"]=$donorTaxData->TaxExemption->name;
    //         $highDonorList[$i]["Email"]=$donorTaxData->TaxExemption->email;
    //         $highDonorList[$i]["Frequently"]=$HighDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
    //         $highDonorList[$i]["Recently"]=$HighDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
    //         $highDonorList[$i]["Monetary"]=$HighDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
    //         $highDonorList[$i]["TotalMarks"]=$HighDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
    //     }

    //     for($i = 0 ; $i<count($LowDonorData);$i++){
    //         //search name,phonenumber, and email [basic details]
    //         $donorTaxData=TaxExemptionsDonor::with('TaxExemption')->where('donorId',$LowDonorData[$i]->DonorAnalysisActiveDonor->donorId)->first();
    //         $lowDonorList[$i]["Id"] = $donorTaxData->TaxExemption->id;
    //         $lowDonorList[$i]["Position"]=$i+1;
    //         $lowDonorList[$i]["Name"]=$donorTaxData->TaxExemption->name;
    //         $lowDonorList[$i]["Email"]=$donorTaxData->TaxExemption->email;
    //         $lowDonorList[$i]["Frequently"]=$LowDonorData[$i]->DonorAnalysisActiveDonor->frequentlyPoint;
    //         $lowDonorList[$i]["Recently"]=$LowDonorData[$i]->DonorAnalysisActiveDonor->recentlyPoint;
    //         $lowDonorList[$i]["Monetary"]=$LowDonorData[$i]->DonorAnalysisActiveDonor->monetaryPoint;
    //         $lowDonorList[$i]["TotalMarks"]=$LowDonorData[$i]->DonorAnalysisActiveDonor->totalMark;
    //     }

    //     $data  = DonorAnalysisLMonthState::with("AddressesState")->get();

    //     for($i=0; $i<count($data);$i++){
    //         $label[$i]=  $data[$i]->AddressesState->code;
    //         $quantity[$i]=  $data[$i]->quantity;
    //     }
        

    //     return $response =[
    //         'bestDonorList' => $BestDonorList,
    //         'highDonorList' => $highDonorList,
    //         'lowDonorList' => $lowDonorList,
    //         'labels' =>$label,
    //         'quantity'=>$quantity
    //     ];
    // }


    public function listTax($id) {

        $data = TaxExemption::with('TaxExemptionsDonor')
        ->where('tax_exemptions.id', $id)
        ->first();

        $allID=TaxExemptionsDonor::where('donorId',$data->TaxExemptionsDonor->donorId)->get();

        $array=0;
        $taxList=null;
        $amount=0;
        $receiptCount=0;
        for($i=0;$i<count($allID);$i++){
            $tax = TaxExemption::with('TaxExemptionsStatus.StatusColor', 'TaxExemptionsType')
            ->orderBy('tax_exemptions.created_at', 'DESC')
            ->where('donorId',$allID[$i]->id)
            ->first();

            $color = StatusColor::where('id',$tax->TaxExemptionsStatus->colorId)->select("status_colors.color")->first();
            $taxList[$i]['array']=$array;
            $taxList[$i]['RequestID']=$tax->id;
            $taxList[$i]['Name']=$tax->name;
            $taxList[$i]['PhoneNumber']=$tax->phoneNumber;
            $taxList[$i]['RequestDate']=$tax->created_at;
            $taxList[$i]['Amount']=$tax->amount;
            $taxList[$i]['Type']=$tax->TaxExemptionsType->type;
            $taxList[$i]['Status']=$tax->TaxExemptionsStatus->status;
            $taxList[$i]['ColorStatus']=$tax->TaxExemptionsStatus->StatusColor->color;
            $amount+=$tax->amount;
            $receiptCount += TaxExemptionsDonationReceipt::where('taxId',$tax->id)->count();
            $array++;
        }
        
        return $response = [
            'taxList' => $taxList,
            'amount' => $amount,
            'receiptCount' =>$receiptCount,
            'totalRequest'=>count($allID),
        ];
    }
}
