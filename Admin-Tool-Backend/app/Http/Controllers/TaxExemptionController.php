<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TaxExemption;
use App\Models\Addresses;
use App\Models\TaxExemptionsDonationReceipt;
use App\Models\TaxExemptionsPostageReceipt;
use App\Models\TaxExemptionsYayasanReceipt;
use App\Models\TaxExemptionsStatus;
use App\Models\TaxExemptionsTrack;
use App\Models\TaxExemptionsTrackHistory;
use App\Models\DonorAnalysisNewDonation;
use App\Models\DonorAnalysisMonthState;
use App\Models\StatusColor;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mails;
use Illuminate\Support\Facades\Storage;
use App\Exports\TaxExemptionsMultisheetsExportExcel;
use App\Exports\TaxExemptionsExportPDF;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\DeclineRequestTaxExemption;
use App\Mail\ReceiveTaxExemptionRequest;
use App\Mail\ReceiptFromCintaSyriaMalaysia;
use Mail;
use ZipArchive;
use File;
use Validator;
use Carbon\Carbon;
use PDF;

class TaxExemptionController
{
    public function index() {        

        $data = TaxExemption::with('TaxExemptionsStatus.StatusColor', 'TaxExemptionsType')
        ->orderBy('tax_exemptions.created_at', 'DESC')
        ->get();

        $countPage=0;
        $array=0;
        $currentPage=1;
        $perPage=10;
        $page=null;
        $taxList=null;
        for($i=0;$i<count($data);$i++){
            $color = StatusColor::where('id',$data[$i]->TaxExemptionsStatus->colorId)->select("status_colors.color")->first();
            $taxList[$i]['selected']=false;
            $taxList[$i]['array']=$array;
            $taxList[$i]['RequestID']=$data[$i]->id;
            $taxList[$i]['Name']=$data[$i]->name;
            $taxList[$i]['PhoneNumber']=$data[$i]->phoneNumber;
            $taxList[$i]['RequestDate']=$data[$i]->created_at;
            $taxList[$i]['Amount']=$data[$i]->amount;
            $taxList[$i]['Type']=$data[$i]->TaxExemptionsType->type;
            $taxList[$i]['Status']=$data[$i]->TaxExemptionsStatus->status;
            $taxList[$i]['ColorStatus']=$data[$i]->TaxExemptionsStatus->StatusColor->color;
            $array++;
            if(($i+1)%$perPage == 0){
                $page[$countPage]['page'] = $currentPage;
                $page[$countPage]['itemCount'] = $perPage;
                $countPage++;
                $currentPage++;
            }
        }

        //calculate for extra page
        $remainingItem = count($data)-($perPage*($currentPage-1));
        if($remainingItem > 0){
                $page[$countPage]['page'] = $currentPage;
                $page[$countPage]['itemCount'] = $remainingItem;
        }

        
        return $response = [
            'page'=>$page,
            'taxList' => $taxList,
        ];
    }

    public function filterTax(Request $request) {        

        $fields = $request->validate([
            'filter'=>'required',
        ]);

        if((int)$fields['filter']==0) {
            $data = TaxExemption::join('tax_exemptions_statuses', 'tax_exemptions_statuses.id', '=', 'tax_exemptions.statusId')
            ->join('status_colors', 'status_colors.id', '=', 'tax_exemptions_statuses.colorId')
            ->join('tax_exemptions_types','tax_exemptions_types.id', '=','tax_exemptions.typeId')
            ->orderBy('tax_exemptions.created_at', 'DESC')
            ->select('tax_exemptions.id',
            'tax_exemptions.name',
            'tax_exemptions.created_at',
            'tax_exemptions.phoneNumber',
            'tax_exemptions.amount',
            'tax_exemptions_statuses.status',
            'color',
            'tax_exemptions_types.type')
            ->orWhere('status', 'like', '%'.$fields['filter'].'%')
            ->orWhere('type', 'like', '%'.$fields['filter'].'%')
            ->orWhere('tax_exemptions.name', 'like', '%'.$fields['filter'].'%')
            ->get();
        }

        if((int)$fields['filter'] > 0) {
            $data = TaxExemption::join('tax_exemptions_statuses', 'tax_exemptions_statuses.id', '=', 'tax_exemptions.statusId')
            ->join('status_colors', 'status_colors.id', '=', 'tax_exemptions_statuses.colorId')
            ->join('tax_exemptions_types','tax_exemptions_types.id', '=','tax_exemptions.typeId')
            ->orderBy('tax_exemptions.created_at', 'DESC')
            ->select('tax_exemptions.id',
            'tax_exemptions.name',
            'tax_exemptions.created_at',
            'tax_exemptions.phoneNumber',
            'tax_exemptions.amount',
            'tax_exemptions_statuses.status',
            'color',
            'tax_exemptions_types.type')
            ->orWhere('amount', 'like', '%'.$fields['filter'].'%')
            ->orWhere('tax_exemptions.id','like', '%'.$fields['filter'].'%')
            ->orWhere('tax_exemptions.phoneNumber', 'like', '%'.$fields['filter'].'%')
            ->get();
        }

        $countPage=0;
        $array=0;
        $currentPage=1;
        $perPage=10;
        $page=null;
        $taxList=null;
        for($i=0;$i<count($data);$i++){
            $taxList[$i]['selected']=false;
            $taxList[$i]['array']=$array;
            $taxList[$i]['RequestID']=$data[$i]["id"];
            $taxList[$i]['Name']=$data[$i]["name"];
            $taxList[$i]['PhoneNumber']=$data[$i]["phoneNumber"];
            $taxList[$i]['RequestDate']=$data[$i]["created_at"];
            $taxList[$i]['Amount']=$data[$i]["amount"];
            $taxList[$i]['Type']=$data[$i]["type"];
            $taxList[$i]['Status']=$data[$i]["status"];
            $taxList[$i]['ColorStatus']=$data[$i]['color'];
            $array++;
            if(($i+1)%$perPage == 0){
                $page[$countPage]['page'] = $currentPage;
                $page[$countPage]['itemCount'] = $perPage;
                $countPage++;
                $currentPage++;
            }
        }

        //calculate for extra page
        $remainingItem = count($data)-($perPage*($currentPage-1));
        if($remainingItem > 0){
                $page[$countPage]['page'] = $currentPage;
                $page[$countPage]['itemCount'] = $remainingItem;
        }
        return $response = [
            'data' => $data,
            'page'=>$page,
            'taxList' => $taxList,
        ];
    }

    public function details($id) {
        $data = TaxExemption::with('TaxExemptionsTrack','TaxExemptionsType','TaxExemptionsDonationReceipt','TaxExemptionsPostage','TaxExemptionsDonor','Addresses.AddressesState','TaxExemptionsStatus.StatusColor','postageAddresses.AddressesState','TaxExemptionsPostageReceipt.TaxExemptionsStatus.StatusColor')->get()->find($id);
        $history = TaxExemptionsTrackHistory::with('TaxExemptionsStatus')
        ->where("taxId", $id)
        ->orderBy("created_at", "desc")
        ->get();
        $filesDonation=$data->TaxExemptionsDonationReceipt;
        $filesPostage= $data->TaxExemptionsPostageReceipt;
        $filePostage=null;
        $shipping=null;

        //insert file in tracking database
        forEach($history as $dataHistory) {
            $fileUrl = $dataHistory->filePath;
            if($dataHistory->filePath != "-" ) {
                $fileUrl = asset($dataHistory->filePath);
            }
            $trackHistory[] = ['Id'=>$dataHistory->id,'Date' => $dataHistory->created_at, 'Status'=>$dataHistory->TaxExemptionsStatus->status, 'Remark'=>$dataHistory->remark, 'Document' => $fileUrl];
        };
   
        if($filesPostage !=null) {
            if($filesPostage->file_path !=null){
                $filePostage = asset($data->TaxExemptionsPostageReceipt->file_path);
            }
        }
        $donationReceipt = [];

        for($i = 0; $i<count($filesDonation);$i++){
            $donationReceipt[$i]["Name"] = "Receipt ".$i+1;
            $donationReceipt[$i]["fileUrl"] = asset($filesDonation[$i]["file_path"]);
            $donationReceipt[$i]["Amount"] = $filesDonation[$i]["amount"];
            $donationReceipt[$i]["Reference"] = $filesDonation[$i]["references"];
        }

        //gather all data that needed at tax exemption details page
        $tax['requestId'] = $data->id;
        $tax['requestDate'] = $data->created_at;
        $tax['name'] = $data->name;
        $tax['donorId'] = $data->TaxExemptionsDonor->donorId;
        $tax['phoneNumber'] = $data->phoneNumber;
        $tax['email'] = $data->email;
        $tax['amount'] = $data->amount;
        $tax['statusId'] = $data->TaxExemptionsStatus->id;
        $tax['statusColor'] = $data->TaxExemptionsStatus->StatusColor->color;
        $tax['status'] = $data->TaxExemptionsStatus->status;
        $tax['typeDonation'] = $data->TaxExemptionsType->type;
        $tax['isPostStatus'] = $data->TaxExemptionsPostage->confirmed;
        $tax['address'] = $data->Addresses->address;
        $tax['city'] = $data->Addresses->city;
        $tax['postalCode'] = $data->Addresses->postalCode;
        $tax['state'] = $data->Addresses->AddressesState->state;
        $tax['trackingNumber'] = $data->TaxExemptionsTrack->trackingNumber;
        if($data->postageAddresses != null ){
            $shipping['address']= $data->postageAddresses->address;
            $shipping['city']= $data->postageAddresses->city;
            $shipping['postalCode']= $data->postageAddresses->postalCode;
            $shipping['state']= $data->postageAddresses->AddressesState->state;
            $shipping['statusPaymentPostage']= $data->TaxExemptionsPostageReceipt->TaxExemptionsStatus->status;
            $shipping['statusPostageColor']= $data->TaxExemptionsPostageReceipt->TaxExemptionsStatus->StatusColor->color;
            $shipping['isApprove']= $data->TaxExemptionsPostageReceipt->isApprove;
        }


        return  $response = [
            'tax1' => $filesPostage,
            'tax' => $tax,
            'shipping' => $shipping,
            'donationReceipt' =>$donationReceipt,
            'postageReceipt' =>$filePostage,
            'trackHistory' =>$trackHistory,
        ];
    }

    public function store(Request $request) {

        $validator = Validator::make($request->all(), [
            'name' => 'string|required',
            'phoneNumber' => 'string|required',
            'email' => 'string|required',
            'address'=>'string|required',
            'city'=>'string|required',
            'stateId'=>'string|required',       
            'postalCode' => 'string|required',           
            'shippingId' => 'string|required',
            'shippingAddress' => 'nullable|string',
            'shippingCity' => 'nullable|string',
            'shippingStateId' => 'nullable|integer',
            'shippingPostalCode'=>'nullable|string',
            'donorId' =>'required|string',
            'totalAmount'=>'string|required',
            'filePath'=>'required',
            'typeId' => 'string|required',
            'postId' => 'string|required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->first()], 422);
        }
        
        $result = DB::transaction (function() use ($request){
                
            $addressId = DB::table('addresses')->insertGetId(
                [
                    'address' =>   $request->address,
                    'postalCode' =>  $request->postalCode,
                    'city' =>  $request->city,
                    'stateId'=> $request->stateId,
                    'created_at' => Carbon::now()
                    ]
            );

            $donorId = DB::table('tax_exemptions_donors')->insertGetId(
                [
                    'donorId' =>   $request->donorId,
                    'created_at' => Carbon::now()
                ]
            );

            $postageId=null;
            if( $request->postId == "1" ){         
                if( $request->shippingId =="0"){
                    $postageId = DB::table('addresses')->insertGetId(
                        [
                            'address' =>  $request->shippingAddress,
                            'postalCode' =>  $request->shippingPostalCode,
                            'city' =>  $request->shippingCity,
                            'stateId'=> $request->shippingStateId,
                            'created_at' => Carbon::now()
                            ]
                    );  
        
                }          
                else {
                $postageId = $addressId;
                }
            }
                
            $taxId = DB::table('tax_exemptions')->insertGetId([
                'name' =>  $request->name,
                'phoneNumber' =>  $request->phoneNumber,
                'email' =>  $request->email,
                'amount'=> $request->totalAmount,
                'typeId' =>  $request->typeId,          
                'statusId' => 1,
                'postId' =>  $request->postId,
                'donorId' => $donorId,
                'addressId' => $addressId,
                'postageAddressId' => $postageId,
                'created_at' => Carbon::now(),
            ]);

            //store receipt
            $files= $request->filePath;
            $amounts=$request->amount;
            $references=$request->reference;
            $count=0;
            forEach($files as $file) {
            $filename  = $file->getClientOriginalName();
            // // $extension = $file->getClientOriginalExtension();
            $receipt = time().'_'.$filename;  
            $file->storeAs('public/donor-receipt', $receipt);
            $filePath = '/storage/donor-receipt/' . $receipt;
            TaxExemptionsDonationReceipt::create(
                ['file_path' =>  $filePath, 'created_at' => Carbon::now(), 'taxId'=> $taxId, 'amount'=>$amounts[$count], 'references'=>$references[$count]]
            );
            $count++;
            }

            //store postage receipt exist or not still store
            //check user do a postage or not
            if($postageId !=null) {
                if( $request->postagePath != "null"){
                    $postageFile = $request->postagePath;
                    $postageFileName  = $postageFile->getClientOriginalName();
                    // // $extension = $file->getClientOriginalExtension();
                    $postageReceipt = time().'_'.$postageFileName;  
                    $postageFile->storeAs('public/donor-receipt', $postageReceipt);
                    $postageFilePath = '/storage/donor-receipt/' . $postageReceipt;
                    TaxExemptionsPostageReceipt::create(
                        ['file_path' =>  $postageFilePath, 'created_at' => Carbon::now(), 'taxId'=> $taxId, 'statusId'=>7]
                    ); 
                }
                else
                {
                    TaxExemptionsPostageReceipt::create(
                        ['created_at' => Carbon::now(), 'taxId'=> $taxId , 'file_path'=> null, 'statusId'=>5]
                    );  
                }
            }
            

            $trackingNumber = "CSM".Str::random(11).(string)$taxId;
            //store tracking
            $trackingData=TaxExemptionsTrack::create([
                'trackingNumber' => strtoupper($trackingNumber),
                'created_at' => Carbon::now(),
                'taxId' => $taxId,
                ]);

            $data =  TaxExemptionsTrackHistory::create([
                'remark' => 'Your request has been created',
                'taxId' => $taxId,
                'statusId' => 1,
            ]);
            
            //store donor to table anaylysis
            DonorAnalysisNewDonation::create([
                'donorId' =>  $request->donorId,
                'taxId' =>$taxId,
            ]);

            $details = [
                'requestId' => $taxId,
                'name' =>  $request->name,
                'trackingNumber' =>$trackingData->trackingNumber,
                'trackingUrl' => $request->trackingUrl.$trackingData->trackingNumber,
            ];

            Mail::to( $request->email)->send(new ReceiveTaxExemptionRequest($details));

            return $trackingData->trackingNumber;

            
        });

        return $response = [
            'trackingNumber' => $result,
        ];
     
    }

    public function decline(Request $request) {
  
        $fields = $request->validate([
            'id'=>'int',
            'email' => 'string|required',
            'reason' => 'string|required',
            // 'filePath[]'=>'mimes:csv,txt,xlx,xls,pdf,png,jpeg|required',
        ]);

        $result = DB::transaction (function() use ($fields , $request){

            $tax = TaxExemption::find($fields['id']);
            $tax->statusId = 4;
            $tax->save();

            $trackData = TaxExemptionsTrack::where('taxId', $fields['id'])->first();

            $details = [
                'requestId' => $tax->id,
                'name' => $tax->name,
                'trackingNumber' =>$trackData->trackingNumber,
                'reason' => $fields['reason'],
            ];

            TaxExemptionsTrackHistory::create([
                'remark' => "Tax declined due to ".$fields['reason'],
                'taxId'=> $fields['id'],
                'statusId'=> 4,
            ]);

            Mail::to($fields['email'])->send(new DeclineRequestTaxExemption($details));

            return $succesful ="Email Sent";
        });

        return $response = [
            'message' =>$result,
        ];
    }

    public function exportExcel($startDate, $endDate){
    
        $dataTenThousand = TaxExemption::select('tax_exemptions.amount','tax_exemptions.id')
        ->WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])
        ->where('tax_exemptions.amount','<=',10000)
        ->get();
        
        $dataMoreTenThousand = TaxExemption::select('tax_exemptions.amount','tax_exemptions.id')
        ->WhereBetween('tax_exemptions.created_at', [$startDate, $endDate])
        ->where('tax_exemptions.amount','>',10000)
        ->get();

        if(count($dataTenThousand) <= 0 && count($dataMoreTenThousand ) <=0){
            return response()->json(['errors'=>"No Data To Export"], 422);
        }

        $taxIdTenThousand= [];
        $taxIdMoreTenThousand= [];
        $totalSheetsTenThousand = 0;
        $totalSheetsMoreTenThousand = 0;
        $totalAmount=0;
        $totalTaxId=0;

        for ($currentRow =0; $currentRow < count($dataTenThousand); $currentRow++)
        {
            $totalAmount += $dataTenThousand[$currentRow]['amount'];
            $taxIdTenThousand[$totalSheetsTenThousand][$totalTaxId]= $dataTenThousand[$currentRow]->id;
            $totalTaxId++;

            if($totalAmount > 10000){       
                $removeLastArray = array_pop($taxIdTenThousand[$totalSheetsTenThousand]); 
                $currentRow -= 1;
                $totalAmount = 0;                
                $totalSheetsTenThousand++;
                $totalTaxId=0;
            }

            else if($totalAmount == 10000){
            
                $totalAmount=0;                
                $totalSheetsTenThousand++;                
                $totalTaxId=0;
            }

            else if($currentRow == (count($dataTenThousand)-1)){
                $totalSheetsTenThousand++;
            }
           
        }
        
        if(count($dataMoreTenThousand)>0){
            for($currentRow = 0 ; $currentRow < count($dataMoreTenThousand); $currentRow ++) {
                $taxIdMoreTenThousand[$totalSheetsMoreTenThousand][0]= $dataMoreTenThousand[$currentRow]->id;
                $totalSheetsMoreTenThousand++;
            }
        }

        //naming the file
        $fileTax=[];
        $fileNameTenThousand = time().'_taxExemption.xlsx';
        $fileTax[0] = [
            "name" =>$fileNameTenThousand,
        ];
        $fileNameMoreTenThousand = time().'_isMoreTenThousand_TaxExemption.xlsx';
            $fileTax[1] = [
                "name" =>$fileNameMoreTenThousand,
            ];
        
        if(count($taxIdTenThousand) > 0 && count($taxIdMoreTenThousand)==0){
            $dataTenThousand = Excel::store(new TaxExemptionsMultisheetsExportExcel($taxIdTenThousand), $fileNameTenThousand , 'excel');

            $path = '/storage/temp/'.$fileNameTenThousand;
            $exists = asset($path);
            return $exists;
        }

        else if(count($taxIdTenThousand) == 0 && count($taxIdMoreTenThousand) > 0) {
            $dataMoreTenThousand = Excel::store(new TaxExemptionsMultisheetsExportExcel($taxIdMoreTenThousand), $fileNameMoreTenThousand , 'excel');

            $path = '/storage/temp/'.$fileNameMoreTenThousand;
            $exists = asset($path);
            return $exists;
        }
     

        if(count($taxIdMoreTenThousand)  > 0 && count($taxIdTenThousand) > 0 ) {            
            $dataTenThousand = Excel::store(new TaxExemptionsMultisheetsExportExcel($taxIdTenThousand), $fileNameTenThousand , 'excel');
            $dataMoreTenThousand = Excel::store(new TaxExemptionsMultisheetsExportExcel($taxIdMoreTenThousand), $fileNameMoreTenThousand , 'excel');

            $zip = new ZipArchive;
            $zipFileName = time()."_taxExemptions.zip";
            $urlFile='app/public/temp/'.$zipFileName;
            if($zip->open(storage_path($urlFile), ZipArchive::CREATE) === TRUE)
            {
                forEach($fileTax as $file) {
                    $relativeNameInZipFile = basename($file["name"]);
                    $theFile = File::files(storage_path('app/public/temp/'));
                    forEach($theFile as $key => $value){
                     if(str_contains($value, $file["name"])){
                         $zip->addFile($value,$relativeNameInZipFile);
                     }               
                    }
                };
                $zip->close();
            };
    
     
         $url ='/storage/temp/'.$zipFileName;    
         $exists = asset($url);    
         return $exists;

        }

        return $dataTenThousand;
 
    }

    public function exportPDF(){

        $dataTenThousand = TaxExemption::join('tax_exemptions_statuses','tax_exemptions_statuses.id','tax_exemptions.statusId')
        ->where('tax_exemptions.statusId',1)
        ->where('tax_exemptions.amount', '<=', 10000)
        ->select('tax_exemptions.amount','tax_exemptions.id')
        ->get();

        $dataMoreTenThousand =TaxExemption::join('tax_exemptions_statuses','tax_exemptions_statuses.id','tax_exemptions.statusId')
        ->where('tax_exemptions.statusId',1)
        ->where('tax_exemptions.amount', '>', 10000)
        ->select('tax_exemptions.id')
        ->get();

        if(count($dataTenThousand) <= 0 && count($dataMoreTenThousand ) <=0){
            return response()->json(['errors'=>"No Data To Export"], 422);
        }

        $taxIdTenThousand= [];
        $taxIdMoreTenThousand= [];
        $totalPDFTenThousand = 0;
        $totalPDFMoreTenThousand = 0;
        $totalAmount=0;
        $totalTaxId=0;

        for ($currentRow =0; $currentRow < count($dataTenThousand); $currentRow++)
        {       
          
            $totalAmount +=$dataTenThousand[$currentRow]['amount'];
            $taxIdTenThousand[$totalPDFTenThousand][$totalTaxId]=$dataTenThousand[$currentRow]->id;
            $totalTaxId++;
    
                if($totalAmount > 10000){       
                    $removeLastArray = array_pop($taxIdTenThousand[$totalPDFTenThousand]); 
                    $currentRow -= 1;
                    $totalAmount = 0;                
                    $totalPDFTenThousand++;
                    $totalTaxId=0;
                }
    
                else if($totalAmount == 10000){
                
                    $totalAmount=0;                
                    $totalPDFTenThousand++;                
                    $totalTaxId=0;
                }
    
                else if($currentRow == (count($dataTenThousand)-1)){
                    $totalPDFTenThousand++;
                }
            
           
        }

        $PDFTenThousandFile = [];
        $PDFMoreTenThousandFile = [];

        if(count($taxIdTenThousand)>0) {
            for($countPDF = 0; $countPDF < $totalPDFTenThousand ; $countPDF++){
                $fileName = 'taxExemptions_'.($countPDF+1).'_'.time().'.pdf';
                $data = Excel::store(new TaxExemptionsExportPDF($taxIdTenThousand[$countPDF]), $fileName , 'excel', \Maatwebsite\Excel\Excel::DOMPDF );
             
                $PDFTenThousandFile[$countPDF] = [
                    "name" =>$fileName,
                ];
            }
        }

        if(count($dataMoreTenThousand) >0){
            for($countPDF = 0; $countPDF <count($dataMoreTenThousand) ; $countPDF++){
                //need to store like this since only accept Array in TaxExemptionsExportPDF
                $taxIdMoreTenThousand[$totalPDFMoreTenThousand][0]= $dataMoreTenThousand[$countPDF]->id;
                $totalPDFMoreTenThousand++;
                $fileName = 'MoreTenThousand_taxExemptions_'.($countPDF+1).'_'.time().'.pdf';
                $data = Excel::store(new TaxExemptionsExportPDF($taxIdMoreTenThousand[$countPDF]), $fileName , 'excel', \Maatwebsite\Excel\Excel::DOMPDF );
             
                $PDFMoreTenThousandFile[$countPDF] = [
                    "name" =>$fileName,
                ];
            }
        }   

        //       return $response = [
        //     "dataTenThousand" => $dataTenThousand,
        //     "dataMoreTenThousand" => $dataMoreTenThousand,
        //     "countdataMore" => count($dataMoreTenThousand),
        //     "datadata" =>  $taxIdMoreTenThousand[0],
        //     "datadataasdsadasd" =>  $taxIdTenThousand[0]
        // ];

        //store in Zip
        if(count($PDFTenThousandFile) > 1 || count($PDFMoreTenThousandFile) >1 || count($PDFTenThousandFile) + count($PDFMoreTenThousandFile) > 1) {
            $zip = new ZipArchive;
            $zipFileName = time()."_taxExemptions.zip";
            $urlFile='app/public/temp/'.$zipFileName;
            if($zip->open(storage_path($urlFile), ZipArchive::CREATE) === TRUE)
            {
                forEach($PDFTenThousandFile as $file) {
                    $relativeNameInZipFile = basename($file["name"]);
                    $theFile = File::files(storage_path('app/public/temp/'));
                    forEach($theFile as $key => $value){
                     if(str_contains($value, $file["name"])){
                         $zip->addFile($value,$relativeNameInZipFile);
                     }               
                    }
                };
                if(count($PDFMoreTenThousandFile)>0){
                 forEach($PDFMoreTenThousandFile as $file) {
                     $relativeNameInZipFile = basename($file["name"]);
                     $theFile = File::files(storage_path('app/public/temp/'));
                     forEach($theFile as $key => $value){
                      if(str_contains($value, $file["name"])){
                          $zip->addFile($value,$relativeNameInZipFile);
                        }               
                    }
                 };
                }
                $zip->close();
            };
     
            $url ='/storage/temp/'.$zipFileName;
        }

        else if(count($PDFTenThousandFile) == 1 && count($PDFMoreTenThousandFile) == 0){
            $url ='/storage/temp/'.$PDFTenThousandFile[0]['name'];
        }
          
        else if(count($PDFMoreTenThousandFile) == 1 && count($PDFTenThousandFile) == 0){
            $url ='/storage/temp/'.$PDFMoreTenThousandFile[0]['name'];
        }

        $exists = asset($url);
 
        return $exists;
    }

    public function updateStatus(Request $request, $id){

        $fields = $request->validate([
            'statusId' => 'required',
            'remark' => ''
        ]);

        $tax = TaxExemption::find($id);
        $tax->statusId = $fields['statusId'];
        $tax->save();

       TaxExemptionsTrackHistory::create([
           'remark' => $fields['remark'],
           'taxId'=> $id,
           'statusId'=> $fields['statusId'],
       ]);

        return $tax;

    }

    public function updateStatusAll(Request $request){

        $fields = $request->validate([
            'statusId' => 'required',
             'remark' => '',
             'selected'=>'required',
        ]);

        $array = json_decode($fields['selected'], true);

        for($i=0;$i<count($array);$i++){
            $data= TaxExemption::where('id', $array[$i]['RequestID'])->update(['statusId' => $fields['statusId']]);
            TaxExemptionsTrackHistory::create([
                        'taxId'=> $array[$i]['RequestID'],
                        'statusId'=> $fields['statusId'],
                        'remark'=>$fields['remark'],
                    ]);
        }

        return "succesfull";
    }

    public function sendReceiptTaxEmail(Request $request){
        $fields = $request->validate([
            'name' => 'string|required',
            'email' => 'string|required',
            'receipt'=>'required|mimes:png,jpg,jpeg,csv,txt,xlx,xls,pdf',
            'taxId'=>'required|int',
        ]);
        $result = DB::transaction (function() use ($fields , $request){

            $file= $request->receipt;
            $filename = $file->getClientOriginalName();
            $receipt = time().'_'.$filename;  
            $file->storeAs('public/yayasan-receipt', $receipt);       
            $filePath = '/storage/yayasan-receipt/' . $receipt;
            TaxExemptionsYayasanReceipt::create(
                ['file_name'=>$receipt, 'file_path' =>  $filePath, 'created_at' => Carbon::now(), 'taxId'=> $fields['taxId']]
            );
            
            $details =[
                'name' => $fields['name'],
                'requestId' => $fields ['taxId'],
            ];

            Mail::to($fields['email'])->send(new ReceiptFromCintaSyriaMalaysia($request->receipt->getRealPath(), $details));

    
           TaxExemptionsTrackHistory::create([
               'remark' => "Send Receipt",
               'taxId'=> $fields ['taxId'],
               'statusId'=> 3,
               'filePath' => $filePath,
           ]);

           $tax = TaxExemption::find($fields ['taxId']);
           $tax->statusId = 3;
           $tax->save();

            return $succesful ="berjaya";
        });

        return $response = [
            'message' =>$result,
        ];

    }
}
