<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemptionsFaq;
use Carbon\Carbon;

class TaxExemptionsFaqController extends Controller
{
    public function index(){

        $FAQs=TaxExemptionsFaq::all();
        for($i=0 ; $i<count($FAQs);$i++){
            $faq[$i]["No"] = $i+1;
            $faq[$i]["Id"] = $FAQs[$i]->id;
            $faq[$i]["Question"] = $FAQs[$i]->question;
            $faq[$i]["Answer"] = $FAQs[$i]->answer;
        } 

        return $response = [
            "faq"=>$faq,
        ];
    }

    public function add(Request $request){
        $fields = $request->validate([
            'question' => 'required|string',
            'answer' => 'required|string'
        ]);

        $faq = TaxExemptionsFaq::create([
            'question' => $fields['question'],
            'answer'=> $fields['answer'],
            'created_at'=> Carbon::now(),
        ]);
 
         return $faq;
    }

    public function delete($id){
       
        return TaxExemptionsFaq::destroy($id);
    }

    public function update($id , Request $request){
        $fields = $request->validate([
            'question' => 'required|string',
            'answer' => 'required|string'
        ]);
       
        $faq = TaxExemptionsFaq::find($id);
        $faq->update($request->all());
        return $faq;
    }
}
