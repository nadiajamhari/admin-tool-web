<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemptionsTerm;
use Carbon\Carbon;

class TaxExemptionsTermController extends Controller
{
    public function index(){
        $Terms=TaxExemptionsTerm::all();
        for($i=0;$i<count($Terms);$i++){
            $term[$i]["No"] = $i+1;
            $term[$i]["Id"] = $Terms[$i]->id;
            $term[$i]["Term"] = $Terms[$i]->term;
            $term[$i]["Key"] = "Terms".$i*10;
        };

        return $response = [
            "term"=>$term,
        ];
    }

    public function add(Request $request){
        $fields = $request->validate([
            'term' => 'required|string',
        ]);

        $term = TaxExemptionsTerm::create([
            'term' => $fields['term'],
            'created_at'=> Carbon::now(),
        ]);
 
         return $term;
    }

    public function delete($id){
       
        return TaxExemptionsTerm::destroy($id);
    }

    public function update($id , Request $request){
        $fields = $request->validate([
            'term' => 'required|string',
        ]);
       
        $term = TaxExemptionsTerm::find($id);
        $term->update($request->all());
        return $term;
    }
}
