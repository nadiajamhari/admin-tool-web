<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TaxExemption;
use App\Models\TaxExemptionsTrack;
use App\Models\TaxExemptionsTrackHistory;
use Illuminate\Support\Facades\DB;

class TaxExemptionsTrackingController extends Controller
{
    
    public function trackTaxWithId($id){
        
        if(!($trackData = TaxExemptionsTrack::where('trackingNumber', $id)->first())){
            return response([
                'error' => 'Tracking number not found'
            ], 401);
        }
        $data = TaxExemption::with('TaxExemptionsType','TaxExemptionsDonor','TaxExemptionsStatus.StatusColor','TaxExemptionsPostage','postageAddresses.AddressesState','TaxExemptionsPostageReceipt.TaxExemptionsStatus.StatusColor')->get()->find($trackData->taxId);
        $history = TaxExemptionsTrackHistory::with('TaxExemptionsStatus')
        ->where("taxId",$trackData->taxId)
        ->orderBy("created_at", "desc")
        ->get();

        $response = [
            'tax' => $data,
            'trackHistory' =>$history,
        ];
        return $response;  
    }
}
