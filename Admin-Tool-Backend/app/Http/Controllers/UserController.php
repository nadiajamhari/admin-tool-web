<?php

namespace App\Http\Controllers;
use Carbon\Carbon;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\UsersRole;
use App\Models\UsersEmail;
use Validator;

class UserController extends Controller
{
    //get all list of user for user.js
    public function index(){

        $data = User::with('UsersRole')->get();
        for($i=0;$i<count($data);$i++){
            $users[$i]["No"] = $i+1;
            $users[$i]["Id"] = $data[$i]->id;
            $users[$i]["StaffID"] = $data[$i]->staffId;
            $users[$i]["Name"] = $data[$i]->name;
            $users[$i]["Email"] = $data[$i]->email;
            $users[$i]["RoleID"] = $data[$i]->roleId;
            $users[$i]["Role"] = $data[$i]->UsersRole->role;
            $users[$i]["RegisteredDate"] = Carbon::parse($data[$i]->created_at)->format('d-m-Y');
        }
        
        return $response = [
            "users"=>$users
        ];
    }

    public function delete($id){
        $data = User::findOrFail($id);
        $data->delete();
        return 204;
    }

    public function registerUser(Request $request){

        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|unique:users,email',
            'role' => 'string|required',
            'staffId'=> 'string|required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->first()], 422);
        }

        $result = DB::transaction (function() use ($request){
            
            //get role Id
            $roleId = UsersRole::where('role', $request->role)->first()->id;

            //store user data
            $user = User::create([
                'name' =>$request->name,
                'email' => $request->email,
                'password' => bcrypt("!Abc123"),
                'roleId' => $roleId,
                'staffId'=>$request->staffId,
                'created_at' => Carbon::now(),
            ]);

            $token = $user->createToken('myapptoken')->plainTextToken;

            //store email data add emails database
            $emailData = UsersEmail::create([
                'email'=>$request->email,
                'created_at'=>Carbon::now(),
                'users_id' =>$user->id,
            ]);
            return $user->id;
        });
        
        return $response = [
            "id" =>$result,
        ];
    }

    public function update (Request $request) {

        $fields = $request->validate([
            'id' => 'integer|required',
            'name' => 'required|string',
            'email' => 'required|email',
            'staffId'=> 'string|required',
        ]);

        // check the email is already taken by others or not
        if($user = User::where('email', $fields['email'])->first()){
            if($fields['id'] != $user->id){
                return response()->json(['error' => 'The email has been used by the other user!'], 401);
                }
        }

        $result = DB::transaction (function() use ($fields){
            
            // get role Id   

            DB::table('users')
            ->where('id', $fields['id'])
            ->update([
                'name' => $fields['name'],
                'email' => $fields['email'],
                'staffId'=>$fields['staffId'],
            ]);

            return "User Successfully Updated";
        });
        return $response = [
            "message" => $result,
        ];
    }

    public function details($id){

        $data= User::with('UsersRole', 'UsersEmail')->get()->find($id);

        $user['name']=$data->name;
        $user['email']=$data->email;
        $user['roleId']=$data->UsersRole->id;
        $user['role']=$data->UsersRole->role;
        $user['staffId']=$data->staffId;

        $emailData = null;
      
        if(count($data->UsersEmail) >0 ){
            for($i=0;$i<count($data->UsersEmail);$i++) {
                $emailData[$i]["No"] = $i+1;
                $emailData[$i]["Id"]=$data->UsersEmail[$i]->id;
                $emailData[$i]["Email"] = $data->UsersEmail[$i]->email;
                $emailData[$i]['Password'] = $data->UsersEmail[$i]->password;
                if($data->UsersEmail[$i]->password != null) {
                    $emailData[$i]["Password"] = "******";
                }
            }
        }
       
        return $response = [
            "user" => $user,
            "email" => $emailData,
        ];

    }

    public function addEmail(Request $request){

        $validator = Validator::make($request->all(), [
            'userId' => 'integer|required',
            'email' => 'required|unique:users_emails',
            'password'=> 'string|required',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->first()], 422);
        }

        $emailData = UsersEmail::create([
            'email'=>$request->email,
            'password'=>Crypt::encryptString($request->password),
            'created_at'=>Carbon::now(),
            'users_id' =>$request->userId,
        ]);

        return $response= [
            "message"=>"Email Successfully Added",
        ];

    }

    public function editEmail(Request $request){


        $validator = Validator::make($request->all(), [
            'emailId' => 'integer|required',
            'userId' => 'integer|required',
            'email' => 'required|email',

        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->first()], 422);
        }

        $result = DB::transaction (function() use ($request){
            
            if($user = User::where('email', $request->email)->first()){
                if($request->userId != $user->id){
                    return response()->json(['errors' => 'The email has been used by the other user!'], 401);
                }
                else {
                    DB::table('users')
                    ->where('id', $request->userId)
                    ->update([
                        'email' => $request->email,
                    ]);
                }
            
            }

            if($request->password !="") {
                DB::table('users_emails')
                ->where('id', $request->emailId)
                ->update([
                    'email' => $request->email,
                    'password'=> Crypt::encryptString($request->password),
                ]);
            }

            else {
                DB::table('users_emails')
                ->where('id', $request->emailId)
                ->update([
                    'email' => $request->email,
                ]);
            }

            return "Email Sucessfully Updated";
        });
        return $response = [
            "message" => $result,
        ];
    }

    public function deleteEmail($id){

        $data = UsersEmail::findOrFail($id);
        $data->delete();
        return $data;
      
    }

    public function showPassword(Request $request){

        $fields = $request->validate([
            'password' => 'string|required',
            'emailId' =>'integer|required',
        ]);

        $currentUser = auth()->user();

        if(!Hash::check($fields['password'], $currentUser->password)) {
            return response([
                'errors' => 'Your current password is invalid'
            ], 401);
        }

        $emailData = UsersEmail::where("id",$fields['emailId'])->first();
        $password = Crypt::decryptString($emailData->password);

        return $response = [
            "message" => "your password correct",
            "password" =>$password,
        ];
    }
}
