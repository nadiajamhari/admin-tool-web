<?php

namespace App\Imports;

use App\Models\AssetItem;
use App\Models\AssetDetail;
use App\Models\AssetQrcode;
use App\Models\AssetCategory;
use App\Models\AssetDepartment;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsErrors;
use Maatwebsite\Excel\Concerns\SkipsFailures;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class AssetItemImport implements ToCollection, WithHeadingRow, WithValidation //, SkipsOnError, SkipsOnFailure
{
    use Importable; //  ,SkipsErrors, SkipsFailures

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $dept = $this->checkDepartment($row['department']);
            $cat = $this->checkCategory($row['category'], $dept);

            $cat_id = $cat->id;
            $prefix = $cat->prefix;

            $cat_count = AssetDetail::where('category_id', $cat_id)->max('category_count') + 1;
            $pad_cate = str_pad($cat_count, 3, '0', STR_PAD_LEFT);

            // check if description exists 
            $details = AssetDetail::where('description', '=', $row['description'])->first();

            if ($details === null) {
                // details does not exists
                $detail = AssetDetail::create([
                    'category_id' => $cat_id,
                    'category_count' => $cat_count,
                    'unit_price' => $row['unit_price'],
                    'total_quantity' => $row['quantity'],
                    'single_use' => $row['single_use'],
                    'description' => $row['description'],
                    'brand' => $row['brand'],
                ]);

                if ($row['single_use'] == false) {
                    for ($i = 0; $i < $row['quantity']; $i++) {
                        $item_count = AssetItem::where('detail_id', $detail->id)->max('item_count') + 1;
                        $fullCode = 'CSM-' . $prefix . '-' . $row['year'] . '-' . $pad_cate . '-' . $item_count;
                        $qrcode_id = $this->generateQrcode($fullCode);
                        AssetItem::create([
                            'status_id' => '1',
                            'qrcode_id' => $qrcode_id,
                            'code' => $fullCode,
                            'detail_id' => $detail->id,
                            'item_count' => $item_count,
                            'year' => $row['year'],
                            'location' => $row['location'],
                            'remark' => $row['remark'],
                        ]);
                    }
                } else {
                    $item_count = 00;
                    $fullCode = 'CSM-' . $prefix . '-' . $row['year'] . '-' . $pad_cate . '-' . $item_count;
                    $qrcode_id = $this->generateQrcode($fullCode);
                    AssetItem::create([
                        'status_id' => '1',
                        'qrcode_id' => $qrcode_id,
                        'code' => $fullCode,
                        'detail_id' => $detail->id,
                        'item_count' => $item_count,
                        'year' => $row['year'],
                        'location' => $row['location'],
                        'remark' => $row['remark'],
                    ]);
                }
            }
        }
    }

    public function checkDepartment($row_department)
    {
        $dept = AssetDepartment::firstOrCreate(['department' => $row_department]);
        return $dept;
    }

    public function checkCategory($row_category, $department)
    {
        $cat = AssetCategory::where(['category' => $row_category])->first();

        if ($cat) {
            return $cat;
        } else {

            /** Prepare prefix if category does not exist */
            $acronym = "";
            if (str_word_count($row_category) == 1) {
                /** Create acronym from first 3 letters */
                $acronym = substr($row_category, 0, 3);
            } else {
                /** Create acronym from first letter of a phrase */
                $acronym = implode('', array_diff_assoc(str_split(ucwords($row_category)), str_split(strtolower($row_category))));
            }

            while (AssetCategory::where(['prefix' => strtoupper($acronym)])->first()) {
                $acronym = "";
                $string = str_replace(' ', '', $row_category);
                $chars = str_split($string, 1);
                shuffle($chars);
                for ($i = 0; $i < 3; $i++) {
                    $acronym .= $chars[$i];
                }
            }
            $cat = AssetCategory::firstOrCreate([
                'department_id' => $department->id,
                'category' => $row_category,
                'prefix' => strtoupper($acronym),
            ]);
            return $cat;
        }
    }

    public function generateQrcode($full_code)
    {
        $file_name = $full_code . '.png';
        $arr = array('full_code' => $full_code);
        $content = json_encode($arr);
        $destinationPath = Storage::path('public/qrcodes/' . $file_name);
        QrCode::format('png')
            ->size(400)
            ->generate($content, $destinationPath);
        $file_path = env('APP_URL') . Storage::url('qrcodes/' . $file_name);
        $data = ['file_name' => $file_name, 'file_path' => $file_path];
        $newQrcode = AssetQrcode::create($data);
        $qrcode_id = $newQrcode->id;
        return $qrcode_id;
    }

    // public function withValidator($validator)
    // {
    //     if ($validator->fails()) {
    //         $errors = $validator->messages();
    //         // print $errors;
    //         // foreach ($errors as $error) {
    //         //     $validator->errors()->add($error, $errors[$error]);
    //         // }
    //         // return $errors;

    //         // return response()->json(['errors' => $errors]);
    //     }
    // }


    public function rules(): array
    {
        return [
            'department' => 'required',
            'category' => 'required',
            'description' => 'required|unique:asset_details',
            'year' => 'required',
            'quantity' => 'required',
            'single_use' => 'required|boolean',
        ];
    }
}
