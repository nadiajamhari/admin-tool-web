<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Addresses extends Model
{
    protected $table = 'addresses';
    protected $primaryKey ='id';
    protected $fillable = [
        'address',
        'postalCode',
        'city',
        'stateId',
      ];

    //each stateId belongsTo state
    public function AddressesState(){
      
      return $this->belongsTo(AddressesState::class, 'stateId');

    }
}
