<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetDepartment extends Model
{
    use HasFactory;

    protected $fillable = [
        'department',
    ];

    // one department has many categories
    public function AssetCategory()
    {
        return $this->hasMany(AssetCategory::class, 'department_id');
    }
}
