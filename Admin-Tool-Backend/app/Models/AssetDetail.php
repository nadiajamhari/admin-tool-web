<?php

namespace App\Models;

use App\Models\AssetItem;
use App\Models\AssetCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssetDetail extends Model
{
    use HasFactory;

    protected $table = "asset_details";

    protected $fillable = [
        'category_id',
        'category_count',
        'description',
        'brand',
        'unit_price',
        'total_quantity',
        'min_quantity',
        'single_use',
    ];

    // one category belongs to ONE department only
    public function AssetCategory()
    {
        return $this->belongsTo(AssetCategory::class, 'category_id');
    }

    // one detail has many items
    public function AssetItem()
    {
        return $this->hasMany(AssetItem::class, 'detail_id');
    }
}
