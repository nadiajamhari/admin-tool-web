<?php

namespace App\Models;

use App\Models\User;
use App\Models\AssetItem;
use App\Models\AssetLogHistory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AssetLog extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'staff_id',
        'item_id',
        'requested_qty',
        'returned_qty',
        'purpose',
        'requested_at',
        'returned_at', 
    ];

    // one log belongs To ONLY ONE staff
    public function User()
    {
        return $this->belongsTo(User::class, 'staff_id');
    }

    // one log contains ONLY ONE item
    public function AssetItem()
    {
        return $this->belongsTo(AssetItem::class, 'item_id');
    }

    // one log has MANY histories
    public function AssetLogHistory()
    {
        return $this->hasMany(AssetLogHistory::class, 'log_id');
    }
}
