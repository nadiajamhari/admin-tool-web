<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'status',
        'color_id',
    ];

    // one status belongs to ONLY ONE color
    public function StatusColor()
    {
        return $this->belongsTo(StatusColor::class , 'color_id');
    }
}
