<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisBroadcastLog extends Model
{
    protected $table = 'donor_analysis_broadcast_logs';
    protected $primaryKey ='id';
    protected $fillable = [
        'type',
      ];

    public function DonorAnalysisAttachment(){
      return $this->hasMany(DonorAnalysisAttachment::class , 'broadcastLogId');
    }

    public function DonorAnalysisContentFile(){
      return $this->hasOne(DonorAnalysisContentFile::class , 'broadcastLogId');
    }

    public function DonorAnalysisReceiver(){
      return $this->hasMany(DonorAnalysisReceiver::class , 'broadcastLogId');
    }
}
