<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisContentFile extends Model
{
    protected $table = 'donor_analysis_content_files';
    protected $primaryKey ='id';
    protected $fillable = [
        'filePath',
        'broadcastLogId'
      ];

      public function DonorAnalysisBroadcastLog(){
        return $this->belongsTo(DonorAnalysisBroadcastLog::class , 'broadcastLogId');
      }
}
