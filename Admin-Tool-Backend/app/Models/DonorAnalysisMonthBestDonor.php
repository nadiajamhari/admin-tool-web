<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisMonthBestDonor extends Model
{
    protected $table = 'donor_analysis_month_best_donors';
    protected $primaryKey ='id';
    protected $fillable = [
        'activeDonorId',
      ];

    public function DonorAnalysisActiveDonor()
    {
        return $this->belongsTo(DonorAnalysisActiveDonor::class , 'activeDonorId');
    }
}
