<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisMonthLowMDonor extends Model
{
    protected $table = 'donor_analysis_month_lowm_donors';
    protected $primaryKey ='id';
    protected $fillable = [
        'activeDonorId',
      ];

    public function DonorAnalysisActiveDonor()
    {
        return $this->belongsTo(DonorAnalysisActiveDonor::class , 'activeDonorId');
    }
}
