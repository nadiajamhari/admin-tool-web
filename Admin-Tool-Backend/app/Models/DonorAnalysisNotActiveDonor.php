<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DonorAnalysisNotActiveDonor extends Model
{
    protected $table = 'donor_analysis_not_active_donors';
    protected $primaryKey ='id';
    protected $fillable = [
        'donorId'
      ];
}
