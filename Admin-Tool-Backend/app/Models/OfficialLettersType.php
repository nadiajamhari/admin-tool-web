<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficialLettersType extends Model
{
    protected $table = 'official_letters_types';
    protected $primaryKey ='id';
    protected $fillable = [
        'type',
      ];
}
