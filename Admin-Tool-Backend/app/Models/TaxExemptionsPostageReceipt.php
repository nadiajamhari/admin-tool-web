<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsPostageReceipt extends Model
{
    protected $table = 'tax_exemptions_postage_receipts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'file_path',
        'taxId',
        'statusId',
        'isApprove',
      ];

      public function TaxExemptionsStatus()
      {
          return $this->belongsTo(TaxExemptionsStatus::class , 'statusId');
      }
}
