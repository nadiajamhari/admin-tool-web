<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsTrackHistory extends Model
{
    protected $table = 'tax_exemptions_track_histories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'taxId',
        'remark',
        'statusId',
        'filePath',
      ];  
      
      public function TaxExemptionsStatus()
      {
          return $this->belongsTo(TaxExemptionsStatus::class , 'statusId');
      }
}
