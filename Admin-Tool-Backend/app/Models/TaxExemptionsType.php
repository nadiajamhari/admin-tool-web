<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxExemptionsType extends Model
{
    protected $table = 'tax_exemptions_types';
    protected $primaryKey ='id';
    protected $fillable = [
        'type',
        'dataType',
        'name'
    ];
}
