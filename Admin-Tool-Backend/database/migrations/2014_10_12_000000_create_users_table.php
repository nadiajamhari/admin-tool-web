<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('staffId')->default('');
            $table->rememberToken();
            $table->unsignedBigInteger('roleId');
            $table->foreign('roleId')->references('id')->on('users_roles')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $data = [
                [
                    'name' => 'Admin',
                    'email' => 'admin@gmail.com',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$x8f/uUau4bRBt5RhpPLUKOuQJUFy9KOSAC7Y1/xHkfEY4w.Rzc5bq',
                    'remember_token' => NULL,
                    'staffId' => '',
                    'roleId' => 1
                ],
                [
                    'name' => 'Staff',
                    'email' => 'staff@gmail.com',
                    'email_verified_at' => NULL,
                    'password' => '$2y$10$x8f/uUau4bRBt5RhpPLUKOuQJUFy9KOSAC7Y1/xHkfEY4w.Rzc5bq',
                    'remember_token' => NULL,
                    'staffId' => 'CSM002',
                    'roleId' => 2
                ]
            ];
            User::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
