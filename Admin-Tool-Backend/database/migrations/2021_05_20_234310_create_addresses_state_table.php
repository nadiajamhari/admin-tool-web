<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\AddressesState;

class CreateAddressesStateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('addresses_states', function (Blueprint $table) {
            $table->id();
            $table->string('state');
            $table->string('code');
            $table->timestamps();
          
        });

        Schema::table('addresses_states', function (Blueprint $table) {
            $data = [
                ["state" => "Johor" , "code" => 'JHR'],
                ["state" => "Kedah" , "code" => 'KDH'],
                ["state" => "Kelantan" , "code" => 'KTN'],
                ["state" => "Kuala Lumpur" , "code" => 'KUL'],
                ["state" => "Labuan" , "code" => 'LBN'],
                ["state" => "Melaka" , "code" => 'MLK'],
                ["state" => "Negeri Sembilan" , "code" => 'NSN'],
                ["state" => "Pahang" , "code" => 'PHG'],
                ["state" => "Perak" , "code" => 'PRK'],
                ["state" => "Perlis" , "code" => 'PLS'],
                ["state" => "Pulau Pinang" , "code" => 'PNG'],
                ["state" => "Putrajaya" , "code" => 'PJY'],
                ["state" => "Sabah" , "code" => 'SBH'],
                ["state" => "Sarawak" , "code" => 'SWK'],
                ["state" => "Selangor" , "code" => 'SGR'],
                ["state" => "Terengganu" , "code" => 'TRG'],
            ];

            AddressesState::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses_states');
    }
}
