<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\DonorAnalysisActivePoint;
use Carbon\Carbon;

class CreateDonorAnalysisActivePoints extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_active_points', function (Blueprint $table) {
            $table->id();
            $table->integer('point');
            $table->integer('group');
            $table->boolean('isActive');
            $table->timestamps();
        });

        Schema::table('donor_analysis_active_points', function (Blueprint $table) {
            $data = [
                ["point" => 100, "group"=> 1, "isActive" => 1, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["point" => 80 , "group"=> 2, "isActive" => 1, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["point" => 60 , "group"=> 3, "isActive" => 1, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["point" => 40 , "group"=> 4, "isActive" => 1, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["point" => 20 , "group"=> 5, "isActive" => 1, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
                ["point" => 0  ,  "group"=> 6, "isActive" => 0, "created_at"=>Carbon::now(), "updated_at"=>Carbon::now()],
            ];

            DonorAnalysisActivePoint::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_analysis_active_points');
    }
}
