<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsType;

class CreateTaxExemptionsTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_types', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('type');
            $table->string('dataType');
            $table->string('name');
        });

        Schema::table('tax_exemptions_types', function (Blueprint $table) {
            $data = [
                ["type" => "Personal", "dataType"=>"Identification Number(IC)",'name' =>'Name'],
                ["type" => "Company", "dataType"=>"Company Number", "name" =>"Company Name"],
            ];

            TaxExemptionsType::insert($data);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_types');
    }
}
