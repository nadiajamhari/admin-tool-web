<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsPostage;

class CreateTaxExemptionsPostagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_postages', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('postStatus');
            $table->boolean('confirmed');
        });

        Schema::table('tax_exemptions_postages', function (Blueprint $table) {
            $data = [
                ["postStatus" => "Yes", "confirmed"=>1],
                ["postStatus" => "No", "confirmed"=>0],
            ];

            TaxExemptionsPostage::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_postages');
    }
}
