<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\TaxExemptionsTerm;

class CreateTaxExemptionsTerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_terms', function (Blueprint $table) {
            $table->id();
            $table->string('term');
            $table->timestamps();
        });

        Schema::table('tax_exemptions_terms', function (Blueprint $table) {
            $data = [
                ["term" => "You must include the receipt of the donation transaction."],
                ["term" => "Contributions eligible for tax exemption are RM 500 and above."],
                ["term" => "Receipts will be provided via email, while postage is based on your request and charges RM 5.00."],
                ["term" => "The tax exemption receipt process takes more than a month."],
            ];

            TaxExemptionsTerm::insert($data);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_terms');
    }
}
