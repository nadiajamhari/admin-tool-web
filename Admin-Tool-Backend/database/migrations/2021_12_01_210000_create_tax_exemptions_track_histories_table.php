<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxExemptionsTrackHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_track_histories', function (Blueprint $table) {
            $table->id();
            $table->string('remark')->nullable();
            $table->unsignedBigInteger('taxId');
            $table->foreign('taxId')->references('id')->on('tax_exemptions')->onDelete('cascade');
            $table->unsignedBigInteger('statusId');
            $table->foreign('statusId')->references('id')->on('tax_exemptions_statuses')->onDelete('cascade');
            $table->string('filePath')->default("-");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_track_histories');
    }
}
