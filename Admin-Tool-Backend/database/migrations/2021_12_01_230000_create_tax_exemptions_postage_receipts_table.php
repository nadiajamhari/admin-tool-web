<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTaxExemptionsPostageReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_exemptions_postage_receipts', function (Blueprint $table) {
            $table->id();
            $table->string('file_path')->nullable();
            $table->boolean('isApprove')->default(0);
            $table->unsignedBigInteger('statusId')->default(7);
            $table->foreign('statusId')->references('id')->on('tax_exemptions_statuses')->onDelete('cascade');
            $table->unsignedBigInteger('taxId');
            $table->foreign('taxId')->references('id')->on('tax_exemptions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tax_exemptions_postage_receipts');
    }
}
