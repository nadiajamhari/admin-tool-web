<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateDonorAnalysisActiveDonors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_active_donors', function (Blueprint $table) {
            $table->id();
            $table->longText('donorId');
            $table->integer('totalDaysFromLastUpdated');
            $table->dateTimeTz('dateLastUpdateAsDonation', $precision = 0);
            $table->integer('frequently');
            $table->integer('monetary');
            $table->integer('recentlyPoint');
            $table->integer('frequentlyPoint');
            $table->integer('monetaryPoint');
            $table->unsignedBigInteger('activeGroup');             
            $table->foreign('activeGroup')->references('id')->on('donor_analysis_active_points')->onDelete('cascade');
            $table->integer('totalMark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('donor_analysis_active_donors');
    }
}
