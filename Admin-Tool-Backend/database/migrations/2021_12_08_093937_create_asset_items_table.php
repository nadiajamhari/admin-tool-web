<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_items', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->integer('year');
            $table->integer('item_count');
            $table->string('location')->nullable();
            $table->string('remark')->nullable();
            $table->unsignedBigInteger('status_id');
            $table->unsignedBigInteger('qrcode_id');
            $table->unsignedBigInteger('detail_id');
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('asset_statuses');
            $table->foreign('qrcode_id')->references('id')->on('asset_qrcodes')->onDelete('cascade');
            $table->foreign('detail_id')->references('id')->on('asset_details')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_items');
    }
}
