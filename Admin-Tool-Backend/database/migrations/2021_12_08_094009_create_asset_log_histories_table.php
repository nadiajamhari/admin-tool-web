<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetLogHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_log_histories', function (Blueprint $table) {
            $table->id();
            $table->string('remark')->nullable();
            $table->unsignedBigInteger('log_id');
            $table->unsignedBigInteger('status_id');
            $table->timestamps();

            $table->foreign('log_id')->references('id')->on('asset_logs')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('asset_statuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_log_histories');
    }
}
