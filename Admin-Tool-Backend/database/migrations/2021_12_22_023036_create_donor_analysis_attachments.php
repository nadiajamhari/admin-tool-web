<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorAnalysisAttachments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_analysis_attachments', function (Blueprint $table) {
            $table->id();
            $table->String("filePath");
            $table->unsignedBigInteger('broadcastLogId');
            $table->foreign('broadcastLogId')->references('id')->on('donor_analysis_broadcast_logs')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_analysis_attachments');
    }
}
