@component('mail::message')

Hi,&nbsp;{{$details['name']}}<br><br>
{!! $details['content'] !!}

@component('mail::signature')
@endcomponent

@endcomponent
