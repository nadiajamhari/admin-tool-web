@component('mail::message')
# Hi, {{$details['name']}}


<p>Resetting your password is easy. Just press the link below and follow the instructions. We'll have you up and running in no time.</p>
    <a href="{{$details['token']}}">Reset Password</a>
@component('mail::signature')
@endcomponent

@endcomponent

