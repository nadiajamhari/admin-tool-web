@component('mail::message')
# Dear {{ $details['staff_name'] }}

<p>This is to inform you that you have been charged with a  penalty
for losing / not returning the following item(s). </p>

<p><strong>Item Code: </strong>{{$details['item_code']}}</p>
<p><strong>Penalty Charges: </strong>RM {{$details['unit_price']}}</p>

<p>Kindly pay the charges to the bank account. </p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
