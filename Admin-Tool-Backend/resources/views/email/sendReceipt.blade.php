@component('mail::message')
# YOUR TAX EXEMPTION REQUEST HAS BEEN APPROVED!

<p><strong>Request ID</strong> #{{$details['requestId']}}</p>
<hr>

Hi, {{$details['name']}},<br><br>
Your tax exemption request has been approved. Please find the attached file in this email.<br><br>
Should you have any enquiries, kindly WhatsApp us at 011- 24407539 (Admin)


@component('mail::signature')
@endcomponent

@endcomponent
