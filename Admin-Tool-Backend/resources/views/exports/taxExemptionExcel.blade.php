<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tax Exemption</title>
</head>
<body>
<div>
<table>
    <thead>
    <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Identification Number / Company Number</th>
        <th>Address</th>
        <th>Date</th>
        <th>Phone Number</th>
        <th>Email</th>
        <th>Amount</th>
        <th>Total Amount</th>
        <th>References</th>        
    </tr>
    </thead>
    <tbody>
    {{ $number = 1 }}
    @foreach($taxs as $tax)
        <tr>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{$number}}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ $tax->name }}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ $tax->TaxExemptionsDonor->donorId}}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ $tax->address}}, {{$tax->postalCode}}, {{$tax->city}},{{@$tax->state}}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ date_format($tax->created_at ,"d/m/Y")}}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ $tax->phoneNumber }}</td>
            <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{ $tax->email }}</td>
            @if(count($tax->TaxExemptionsDonationReceipt) > 1)
                @foreach(($tax->TaxExemptionsDonationReceipt) as $receipt)
                    @if($loop->first)
                        <td>{{$receipt->amount}}</td>
                        <td rowspan={{count($tax->TaxExemptionsDonationReceipt)}}>{{$tax->amount}}</td>
                        <td>{{$receipt->references}}</td>
                        </tr>
                    @else
                        <tr>
                            <td>{{$receipt->amount}}</td>
                            <td>{{$receipt->references}}</td>
                        </tr>
                    @endif
                @endforeach
            @else
                @foreach(($tax->TaxExemptionsDonationReceipt) as $receipt)
                    <td>{{$receipt->amount}}</td>
                    <td>{{$tax->amount}}</td>
                    <td>{{$receipt->references}}</td>
                    </tr>
                @endforeach
            @endif
            {{$number = $number + 1}}
    @endforeach
    </tbody>
</table>
</div>
</body>
</html>


