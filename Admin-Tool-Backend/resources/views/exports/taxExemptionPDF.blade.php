<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tax Exemption</title>
    <style type="text/css" media="all">
    table 
    { 
        page-break-inside:auto 
    }
    th , td    
    { 
        page-break-inside:avoid; 
        page-break-after:auto
   
    }    
    thead 
    { 
        display:table-header-group 
    }
    tfoot 
    { 
        display:table-footer-group 
    }

    tbody tr td {
        margin-left:15px !important;
    }

    
</style>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>No.</th>
        <th>Name</th>
        <th>Identification Number / Company Number</th>
        <th>Address</th>
        <th>Phone Number</th>
        <th>Email</th>
        <th>Total Amount</th>    
    </tr>
    </thead>
    <tbody>
    {{ $number = 1 }}
    @foreach($taxs as $tax)
        <tr>
            <td>{{$number}}</td>
            <td>{{ $tax->name }}</td>
            <td> {{ $tax->TaxExemptionsDonor->donorId}}</td>
            <td>{{ $tax->address}},&nbsp;{{$tax->postalCode}},&nbsp;{{$tax->city}},&nbsp;{{$tax->state}}</td>
            <td>{{ $tax->phoneNumber }}</td>
            <td>{{ $tax->email }}</td>
                    <td>{{$tax->amount}}</td>
                    </tr>
            {{$number = $number + 1}}
    @endforeach
    </tbody>
</table>
</body>
</html>


