<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\DownloadController;
//test
use App\Http\Controllers\DonorAnalysisController;
use App\Http\Controllers\DonorAnalysisBroadcastLogController;

#region Auth
use App\Http\Controllers\AuthController;
#endregion

#region User
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsersRoleController;
#endregion

#region Address
use App\Http\Controllers\AddressesStateController;
#endregion

#region tax exemption
use App\Http\Controllers\TaxExemptionController;
use App\Http\Controllers\TaxExemptionsTypeController;
use App\Http\Controllers\TaxExemptionsPostageController;
use App\Http\Controllers\TaxExemptionsStatusController;
use App\Http\Controllers\TaxExemptionsFaqController;
use App\Http\Controllers\TaxExemptionsTermController;
use App\Http\Controllers\TaxExemptionsTrackingController;
#endregion

#region asset
use App\Http\Controllers\AssetCategoryController;
use App\Http\Controllers\AssetQrcodeController;
use App\Http\Controllers\AssetStatusController;
use App\Http\Controllers\AssetDepartmentController;
use App\Http\Controllers\AssetLogController;
use App\Http\Controllers\AssetDetailController;
use App\Http\Controllers\AssetItemController;
#endregion

#region statusColor
use App\Http\Controllers\StatusColorController;
#endregion

#region OfficialLetter
use App\Http\Controllers\OfficialLettersTypeController;
#endregion

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//test route for donoranalysis
Route::get('/donorAnalysisAlgorithm', [DonorAnalysisController::class, 'donorAnalysisAlgorithm']);
Route::get('/lastMonth', [DonorAnalysisController::class, 'lastMonth']);

// Public route
//Auth
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/forgotPassword', [AuthController::class, 'forgotPasswordEmail'])->name('email');
Route::post('/resetPassword', [AuthController::class, 'updatePasswordByToken']);
Route::get('/checkToken', [AuthController::class, 'checkToken']);

//TaxExemption
Route::post('/storeTax', [TaxExemptionController::class, 'store']);
Route::get('/getTypeTax', [TaxExemptionsTypeController::class, 'index']);
Route::get('/getPostage', [TaxExemptionsPostageController::class, 'index']);
Route::get('/trackTax/{id}', [TaxExemptionsTrackingController::class, 'trackTaxWithId']);
Route::post('/uploadReceipt', [TaxExemptionsPostageController::class, 'uploadPostageReceipt']);
Route::get('/getAllFaqs', [TaxExemptionsFaqController::class, 'index']);
Route::get('/getAllTerms', [TaxExemptionsTermController::class, 'index']);


//User Role
Route::post('/storeRole', [UsersRoleController::class, 'store']);

//State
Route::get('/allState', [AddressesStateController::class, 'index']);


// Protected routes - need to be authenticated
Route::group(['middleware' => ['auth:sanctum']], function () {

    Route::post('/download/{url}', [DownloadController::class, 'download']);

    #region Auth
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/changePassword', [AuthController::class, 'changePassword']);
    #endregion

    #region TaxExemption
    Route::get('/taxExemptions', [TaxExemptionController::class, 'index']);
    Route::post('/declineTax', [TaxExemptionController::class, 'decline']);
    Route::get('/taxDetails/{id}', [TaxExemptionController::class, 'details']);
    Route::put('/updateStatusTax/{id}', [TaxExemptionController::class, 'updateStatus']);
    Route::post('/updateStatusTaxs', [TaxExemptionController::class, 'updateStatusAll']);
    Route::get('/exportExcelTax/{startDate}/{endDate}', [TaxExemptionController::class, 'exportExcel']);
    Route::get('/exportPDFTax', [TaxExemptionController::class, 'exportPDF']);
    Route::post('/sendReceiptTaxEmail', [TaxExemptionController::class, 'sendReceiptTaxEmail']);
    Route::post('/filterTaxExemption', [TaxExemptionController::class, 'filterTax']);
    Route::put('/updatePostageStatus/{id}', [TaxExemptionsPostageController::class, 'validation']);
    Route::get('/statusTaxExemptions', [TaxExemptionsStatusController::class, 'index']);
    #endregion

    #region User
    Route::get('/allUsers', [UserController::class, 'index']);
    Route::delete('/deleteUser/{id}', [UserController::class, 'delete']);
    Route::post('/addNewUser', [UserController::class, 'registerUser']);
    Route::post('/updateUser', [UserController::class, 'update']);
    Route::get('/userDetails/{id}', [UserController::class, 'details']);
    Route::post('/addEmail', [UserController::class, 'addEmail']);
    Route::delete('/deleteEmail/{id}', [UserController::class, 'deleteEmail']);
    Route::post('/editEmail', [UserController::class, 'editEmail']);
    Route::post('/getPassword',[UserController::class, 'showPassword']);


    //User Role
    Route::get('/getRole', [UsersRoleController::class, 'getRole']);
    #endregion

    #region Address
    Route::post('/storeState', [AddressesStateController::class, 'store']);
    #endregion

    #region Assets
    Route::get('/getCategory', [AssetCategoryController::class, 'index']);
    Route::post('/addCategory', [AssetCategoryController::class, 'store']);
    // Route::get('/getCategory/{id}', [AssetCategoryController::class, 'show']);
    Route::put('/updateCategory/{id}', [AssetCategoryController::class, 'update']);
    Route::delete('/deleteCategory/{id}', [AssetCategoryController::class, 'destroy']);
    Route::post('/categoryAcronym', [AssetCategoryController::class, 'acronym']);

    Route::get('/getDepartment', [AssetDepartmentController::class, 'index']);
    // Route::get('/getDepartment/{id}', [AssetDepartmentController::class, 'show']);
    Route::post('/addDepartment', [AssetDepartmentController::class, 'store']);
    Route::delete('/deleteDepartment/{id}', [AssetDepartmentController::class, 'destroy']);
    Route::put('/updateDepartment/{id}', [AssetDepartmentController::class, 'update']);
    Route::get('/categoriesByDept/{id}', [AssetDepartmentController::class, 'categoriesByDept']);

    Route::post('/generateQrcode', [AssetQrcodeController::class, 'generate']);
    Route::get('/getQrcode', [AssetQrcodeController::class, 'index']);
    // Route::get('/getQrcode/{id}', [AssetQrcodeController::class, 'show']);
    // Route::delete('/deleteQrcode/{id}', [AssetQrcodeController::class, 'destroy']);
    Route::get('/downloadQrcode/{id}', [AssetQrcodeController::class, 'download']);

    Route::resource('assetDetail', AssetDetailController::class);
    Route::resource('assetItem', AssetItemController::class);
    Route::get('/exportTemplate', [AssetItemController::class, 'exportExcelTemplate']);
    Route::post('/importAsset', [AssetItemController::class, 'importFromExcel']);
    Route::put('/updateStatus', [AssetItemController::class, 'updateStatus']);
    Route::get('/itemsByDetail/{id}', [AssetItemController::class, 'itemsByDetail']);


    Route::post('/logRequest', [AssetLogController::class, 'request']);
    Route::put('/logReturn', [AssetLogController::class, 'return']);
    Route::get('/getLog/{code}', [AssetLogController::class, 'show']);
    Route::get('/getLogs', [AssetLogController::class, 'index']);
    Route::get('/getStaffItem', [AssetLogController::class, 'showByStaff']);
    Route::get('/getToReturnItem', [AssetLogController::class, 'showToReturn']);
    Route::get('/stockIndicator', [AssetLogController::class, 'stockPredictor']);
    Route::put('/verifyReturned', [AssetLogController::class, 'verifyReturnedItem']);
    Route::post('/sendPenaltyNotice', [AssetLogController::class, 'sendPenaltyNotice']);
    Route::get('/checkLogStatus', [AssetLogController::class, 'checkLogStatus']);
    #endregion

    #region Status
    Route::get('/statusColor', [StatusColorController::class, 'index']);
    Route::put('/updateStatusTaxRequest/{id}', [TaxExemptionsStatusController::class, 'update']);
    Route::post('/addStatusTaxRequest', [TaxExemptionsStatusController::class, 'add']);
    Route::delete('/deleteStatusTaxRequest/{id}', [TaxExemptionsStatusController::class, 'delete']);
    Route::get('/loadStatus', [TaxExemptionsStatusController::class, 'loadStatus']);

    Route::get('/getAssetStatus', [AssetStatusController::class, 'index']);
    Route::post('/addAssetStatus', [AssetStatusController::class, 'store']);
    Route::put('/updateAssetStatus/{id}', [AssetStatusController::class, 'update']);
    Route::delete('/deleteAssetStatus/{id}', [AssetStatusController::class, 'destroy']);
    #endregion

    #region FAQTax
    Route::post('/addFaq', [TaxExemptionsFaqController::class, 'add']);
    Route::delete('/deleteFaq/{id}', [TaxExemptionsFaqController::class, 'delete']);
    Route::put('/updateFaq/{id}', [TaxExemptionsFaqController::class, 'update']);
    #endregion

    #region TermTax
    Route::post('/addTerm', [TaxExemptionsTermController::class, 'add']);
    Route::delete('/deleteTerm/{id}', [TaxExemptionsTermController::class, 'delete']);
    Route::put('/updateTerm/{id}', [TaxExemptionsTermController::class, 'update']);
    #endregion

    #region DonorAnalysis
    Route::get('/getDonorRanking', [DonorAnalysisController::class, 'index']);
    Route::get('/getTopTenDonorList/{type}', [DonorAnalysisController::class, 'topTenDonor']);
    Route::post('/sendBroadcastEmail', [DonorAnalysisBroadcastLogController::class, 'sendBroadcastEmail']);
    Route::get('/getBroadcastLog',[DonorAnalysisBroadcastLogController::class, 'index']);
    Route::get('/getDonorStateByDate/{startDate}/{endDate}', [DonorAnalysisController::class, 'donorStateByDate']);
    Route::get('/getDonorLastMonth', [DonorAnalysisController::class, 'AllLastMonth']);
    Route::get('/listTaxExemption/{id}', [DonorAnalysisController::class, 'listTax'] );
    #endregion

    #region OfficialLetter
    Route::get('/getOfficialLettersType', [OfficialLettersTypeController::class, 'index']);
    #endregion
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
