import React from "react";
import { Redirect } from "react-router-dom";
import { TheContent, TheSidebar, TheFooter, TheHeader } from "./index";

const TheLayout = (props) => {
  if (localStorage.getItem("token") != null) {
    return (
      <div className="c-app c-default-layout">
        <TheSidebar role={props.role} />
        <div className="c-wrapper">
          <TheHeader />
          <div className="c-body">
            <TheContent role={props.role} />
          </div>
          <TheFooter />
        </div>
      </div>
    );
  }
  return (
    <Redirect
      to={{
        pathname: "/login",
        state: { reason: "You are not logged in. Please login first." },
      }}
    />
  );
};

export default TheLayout;
