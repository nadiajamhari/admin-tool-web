import React from "react";
import { CJumbotron } from "@coreui/react";

const TheHeader = () => {
  return (
    <div>
      <CJumbotron className="tax-header">
        <h1 className="text-center" style={{ color: "white" }}>
          TAX EXEMPTION
        </h1>
      </CJumbotron>
    </div>
  );
};

export default TheHeader;
