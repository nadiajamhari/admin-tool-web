import React from "react";
import TheHeader from "./TheHeader";
import TheSidebar from "./TheSidebar";
import TheContent from "./TheContent";
import TheSubHeader from "./TheSubHeader";
import TheFooter from "./TheFooter";

const TaxExemption = () => {
  return (
    <div className="c-default-layout tax">
      <TheHeader />
      <div className="c-wrapper">
        <div className="row">
          <div className="col-md-3">
            <TheSidebar />
          </div>
          <div className="col-md-9">
            <TheSubHeader />
            <div className="c-body">
              <TheContent />
            </div>
          </div>
        </div>
      </div>
      <TheFooter />
    </div>
  );
};

export default TaxExemption;
