import React from "react";
import Roles from "./services/Roles";

const Dashboard = React.lazy(() => import("./views/dashboard/Dashboard"));

//new page
const TaxExemption = React.lazy(() =>
  import("./views/admin-components/TaxExemptions/TaxExemption")
);

const ChangePassword = React.lazy(() =>
  import("./views/admin-components/ChangePassword/ChangePassword")
);
const Assets = React.lazy(() =>
  import("./views/admin-components/organisationAssets/ViewListAssets")
);
const AssetDetails = React.lazy(() =>
  import("./views/admin-components/organisationAssets/AssetDetails")
);
const AssetAttribute = React.lazy(() =>
  import("./views/admin-components/organisationAssets/Manage")
);
const AssetStatus = React.lazy(() =>
  import(
    "./views/admin-components/SystemSettings/OrganisationAssets/StatusAsset"
  )
);

const TaxExemptionDetails = React.lazy(() =>
  import("./views/admin-components/TaxExemptions/TaxExemptionsDetails")
);

const TaxExemptionStatus = React.lazy(() =>
  import(
    "./views/admin-components/SystemSettings/TaxExemptions/StatusTaxRequest"
  )
);

const TaxExemptionFAQs = React.lazy(() =>
  import("./views/admin-components/SystemSettings/TaxExemptions/FaqTaxRequest")
);

const TaxExemptionTerms = React.lazy(() =>
  import("./views/admin-components/SystemSettings/TaxExemptions/TermTaxRequest")
);

const Users = React.lazy(() => import("./views/admin-components/Users/Users"));
const UserDetails = React.lazy(() =>
  import("./views/admin-components/Users/UserDetails")
);
const DonorAnalysis = React.lazy(() =>
  import("./views/admin-components/DonorAnalysis/DonorRanking/DonorAnalysis")
);

const DonorStatistic = React.lazy(() =>
  import("./views/admin-components/DonorAnalysis/DonorStatistic/DonorStatistic")
);

const TopTenDonor = React.lazy(() =>
  import("./views/admin-components/DonorAnalysis/DonorRanking/TopTenDonorPage")
);

const DonorDetails = React.lazy(() =>
  import("./views/admin-components/DonorAnalysis/DonorRanking/DonorDetails")
);

const BroadcastLog = React.lazy(() =>
  import("./views/admin-components/DonorAnalysis/BroadcastLog/BroadcastLog")
);
// staff component
const StaffDashboard = React.lazy(() =>
  import("./views/staff-components/OrganisationAssets/StaffDashboard")
);
const RequestPage = React.lazy(() =>
  import("./views/staff-components/OrganisationAssets/RequestPage")
);
const ReturnPage = React.lazy(() =>
  import("./views/staff-components/OrganisationAssets/ReturnPage")
);
const RequestHistory = React.lazy(() =>
  import("./views/staff-components/OrganisationAssets/RequestHistory")
);
const RequestOfficalLetter = React.lazy(() =>
  import("./views/staff-components/OfficialLetter/RequestOfficialLetter")
);

const routes = [
  // { path: "/", exact: true, name: "Home" },
  { path: "/admin", exact: true, name: "Home" },
  { path: "/staff", exact: true, name: "Home" },
  {
    path: "/admin/dashboard",
    name: "Dashboard",
    component: Dashboard,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-exemptions",
    name: "Tax Exemption",
    component: TaxExemption,
    exact: true,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-exemptions/tax-exemption-details/:id",
    name: "Tax Exemption Details",
    component: TaxExemptionDetails,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/change-password",
    name: "Change Password",
    component: ChangePassword,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/assets", // /inventory
    exact: true,
    name: "Assets",
    component: Assets,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/assets/details/:id",
    exact: true,
    name: "Asset Details",
    component: AssetDetails,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/asset-attributes",
    exact: true,
    name: "Asset Attributes",
    component: AssetAttribute,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/asset-status",
    name: "Asset Status",
    component: AssetStatus,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-request-status",
    name: "Tax Request Status",
    component: TaxExemptionStatus,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-request-status",
    name: "Tax Request Status",
    component: TaxExemptionStatus,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-request-FAQ",
    name: "Tax Request Frequently Ask Questions",
    component: TaxExemptionFAQs,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/tax-request-terms",
    name: "Tax Request Terms and Conditions",
    component: TaxExemptionTerms,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/users",
    exact: true,
    name: "Users",
    component: Users,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/users/user-details/:id",
    exact: true,
    name: "User Details",
    component: UserDetails,
    permission: [Roles.ADMIN],
  },
  { path: "/admin/users", exact: true, name: "Users", component: Users },
  {
    path: "/admin/donor-statistic",
    name: "Donor Statistic",
    exact: true,
    component: DonorStatistic,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/donor-analysis",
    name: "Donor Analysis",
    exact: true,
    component: DonorAnalysis,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/donor-analysis/:type",
    name: "Top 10 Donors",
    exact: true,
    component: TopTenDonor,
    permission: [Roles.ADMIN],
  },

  {
    path: "/admin/donor-analysis-details/:id",
    name: "Donor Details",
    component: DonorDetails,
    permission: [Roles.ADMIN],
  },
  {
    path: "/admin/broadcast-log",
    name: "Broadcast Log",
    exact: true,
    component: BroadcastLog,
    permission: [Roles.ADMIN],
  },
  /** END ROUTES */

  /** STAFF ROUTES */
  {
    path: "/staff/dashboard",
    name: "Dashboard",
    component: StaffDashboard,
    permission: [Roles.STAFF],
  },
  {
    path: "/staff/request-asset",
    name: "Request Asset Item",
    component: RequestPage,
    permission: [Roles.STAFF],
  },
  {
    path: "/staff/return-asset",
    name: "Return Asset Form",
    component: ReturnPage,
    permission: [Roles.STAFF],
  },
  {
    path: "/staff/request-history",
    name: "Request History",
    component: RequestHistory,
    permission: [Roles.STAFF],
  },
  {
    path: "/staff/request-letter",
    name: "Request Official Letter",
    component: RequestOfficalLetter,
    permission: [Roles.STAFF],
  },
  /** END ROUTES */
];

export default routes;
