import axios from "axios";

const apiClient = axios.create({
  baseURL: "https://fyp2021-api.work-njay.com", //https://api.csm.nadofficial.com https://api.admin.cintasyriamalaysia.com  http://localhost:8000
  withCredentials: true,
  headers: {
    Authorization: "Bearer " + localStorage.getItem("token"),
  },
});

export default apiClient;
