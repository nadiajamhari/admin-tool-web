import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CLink,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import Loader from "../../../../containers/Loader";
import DetailsModal from "./DetailsModal";
import api from "../../../../services/api";

const fields = ["No", "Type", "Content", "SentDate", "Action"];
const LinkStyle = {
  textDecoration: "none",
  color: "#768192",
  fontWeight: 600,
};

class BroadcastLog extends Component {
  constructor() {
    super();
    this.state = {
      loading: true,
      broadcastLogs: [],
      currentReceiver: [],
      currentAttachment: [],
      currentIndex: null,
      detailsModal: false,
    };

    this.toggleHover = this.toggleHover.bind(this);
    this.toggleOutHover = this.toggleOutHover.bind(this);
  }

  componentDidMount() {
    this.getBroadcastLog();
  }

  getBroadcastLog() {
    api.get("/api/getBroadcastLog").then((response) => {
      this.setState({
        broadcastLogs: response.data.broadcast,
        loading: false,
      });
    });
  }

  detailsButton(No) {
    this.setState(
      {
        currentIndex: No - 1,
      },
      () => {
        this.getDetails();
        this.detailsModalForm();
      }
    );
  }

  detailsModalForm() {
    this.setState({
      detailsModal: !this.state.detailsModal,
    });
  }

  getDetails() {
    this.setState({
      currentReceiver:
        this.state.broadcastLogs[parseInt(this.state.currentIndex)].Receiver,
      currentAttachment:
        this.state.broadcastLogs[parseInt(this.state.currentIndex)].Attachment,
    });
  }

  toggleHover = (e) => {
    e.target.style.color = "#3c4b64";
  };

  toggleOutHover = (e) => {
    e.target.style.color = "#768192";
  };

  render() {
    if (this.state.loading === true) {
      return <Loader />;
    } else if (this.state.loading === false) {
      return (
        <div>
          <CCard className="broadcastLog">
            <CCardHeader>
              <h3>Broadcast Log</h3>
            </CCardHeader>
            <CCardBody>
              <CDataTable
                items={this.state.broadcastLogs}
                fields={fields}
                itemsPerPage={10}
                scopedSlots={{
                  Content: (item) => (
                    <td>
                      <CLink
                        href={item.Content}
                        style={LinkStyle}
                        target="_blank"
                        onMouseEnter={this.toggleHover}
                        onMouseLeave={this.toggleOutHover}
                      >
                        <CIcon name="cil-file" className="mfe-2" />
                        Content
                      </CLink>
                    </td>
                  ),
                  Action: (item) => (
                    <td>
                      <CButton
                        color="dark"
                        variant="outline"
                        onClick={this.detailsButton.bind(this, item.No)}
                      >
                        Details
                      </CButton>
                    </td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
          <DetailsModal
            detailsModal={this.state.detailsModal}
            detailsModalForm={this.detailsModalForm.bind(this)}
            receivers={this.state.currentReceiver}
            attachments={this.state.currentAttachment}
            toggleHover={this.toggleOutHover}
            toggleOutHover={this.toggleOutHover}
          />
        </div>
      );
    }
  }
}

export default BroadcastLog;
