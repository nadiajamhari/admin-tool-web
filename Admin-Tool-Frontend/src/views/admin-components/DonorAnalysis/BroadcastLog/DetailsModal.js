import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CLink,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

const LinkStyle = {
  textDecoration: "none",
  color: "#768192",
  fontWeight: 600,
};

class DetailsModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <>
        <CModal
          alignment="center"
          show={this.props.detailsModal}
          onClose={this.props.detailsModalForm}
          closeOnBackdrop={false}
          className="detailsBroadcastModal"
        >
          <CModalHeader closeButton>
            <CModalTitle>Details</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <h4>Receivers</h4>
            {this.props.receivers.map((data) => (
              <p value={data.No} key={data.Email}>
                {data.No}. Name: {data.Name}, Email:{data.Email}
              </p>
            ))}
            {this.props.attachments.length > 0 && (
              <div>
                <h4>Attachment(s)</h4>
                {this.props.attachments.map((data) => (
                  <CLink
                    href={data.url}
                    style={LinkStyle}
                    target="_blank"
                    onMouseEnter={this.props.toggleHover}
                    onMouseLeave={this.props.toggleOutHover}
                  >
                    <CIcon name="cil-file" className="mfe-2" />
                    Content
                  </CLink>
                ))}
              </div>
            )}
          </CModalBody>
        </CModal>
      </>
    );
  }
}

export default DetailsModal;
