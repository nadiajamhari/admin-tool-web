import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CRow,
  CCol,
  CWidgetDropdown,
  CBadge,
  CDataTable,
} from "@coreui/react";
import api from "src/services/api";
import dateFormat from "dateformat";
import Loader from "../../../../containers/Loader";

const fields = [
  "RequestID",
  "Name",
  "RequestDate",
  "PhoneNumber",
  "Amount",
  "Type",
  "Status",
];

class DonorDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taxExemptions: [],
      loading: true,
      amount: "",
      receiptCount: "",
      totalRequest: "",
    };
  }

  componentDidMount() {
    this.getTaxList();
  }

  getTaxList() {
    api
      .get("api/listTaxExemption/" + this.props.match.params.id)
      .then((response) => {
        this.setState({
          taxExemptions: response.data.taxList,
          amount: response.data.amount,
          receiptCount: response.data.receiptCount,
          totalRequest: response.data.totalRequest,
          loading: false,
        });
      })
      .catch((error) => {
        console.log(error.response.data.message);
      });
  }

  detailPage(id) {
    window.location.hash = "/admin/tax-exemptions/tax-exemption-details/" + id;
  }

  render() {
    if (this.state.loading === true) return <Loader />;
    if (this.state.loading === false)
      return (
        <div>
          <CCard>
            <CCardHeader>
              <CRow>
                <CCol>
                  <h5>History of Donor's Tax Request</h5>
                </CCol>
                <CCol className="d-grid gap-2 d-md-flex justify-content-md-end"></CCol>
              </CRow>
            </CCardHeader>
            <CCardBody>
              <CRow className="justify-content-center">
                <CCol sm="6" lg="3">
                  <CWidgetDropdown
                    className="taxCount"
                    color={"gradient-success"}
                    header={this.state.amount}
                    text={"Total Money"}
                  ></CWidgetDropdown>
                </CCol>

                <CCol sm="6" lg="3">
                  <CWidgetDropdown
                    className="taxCount"
                    color={"gradient-success"}
                    header={this.state.totalRequest}
                    text={"Total Tax Exemption"}
                  ></CWidgetDropdown>
                </CCol>

                <CCol sm="6" lg="3">
                  <CWidgetDropdown
                    className="taxCount"
                    color={"gradient-success"}
                    header={this.state.receiptCount}
                    text={"Total Receipt"}
                  ></CWidgetDropdown>
                </CCol>
              </CRow>

              <CDataTable
                fields={fields}
                itemsPerPage={10}
                pagination
                items={this.state.taxExemptions}
                clickableRows
                onRowClick={(item) => this.detailPage(item.RequestID)}
                scopedSlots={{
                  Status: (item) => (
                    <td>
                      <CBadge color={item.ColorStatus}>{item.Status}</CBadge>
                    </td>
                  ),
                  RequestDate: (item) => (
                    <td>{dateFormat(item.RequestDate, "dd/mm/yyyy")}</td>
                  ),
                }}
              />
            </CCardBody>
          </CCard>
        </div>
      );
  }
}

export default DonorDetails;
