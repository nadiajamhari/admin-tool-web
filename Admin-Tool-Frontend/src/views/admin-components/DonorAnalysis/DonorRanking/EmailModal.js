import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CFormGroup,
  CLabel,
  CInputFile,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import EmailForm from "../../SunEditor/ContentForm";

class EmailModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  deleteFilebyId(id) {
    this.props.deleteFile(id);
  }

  render() {
    return (
      <>
        <CModal
          alignment="center"
          show={this.props.emailModal}
          onClose={this.props.emailModalForm}
          closeOnBackdrop={false}
          className="updateStatusModal"
        >
          <CModalHeader closeButton>
            <CModalTitle>Send Email</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <EmailForm
              handleContent={this.props.handleContent}
              content={this.props.content}
            />
            <CFormGroup>
              <CLabel>File</CLabel>
              <CInputFile
                id="filePath"
                name="filePath"
                type="file"
                onChange={this.props.handleFile}
              />
            </CFormGroup>
            {this.props.previewFile.map((file) => (
              <p value={file.id} key={file.file.toString() + file.id}>
                {file.file} &nbsp;
                <CIcon
                  color="danger"
                  name="cil-x"
                  onClick={this.deleteFilebyId.bind(this, file.id)}
                />
              </p>
            ))}
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.props.emailModalForm}>
              Cancel
            </CButton>
            <CButton color="success" onClick={this.props.sendEmail}>
              Send
            </CButton>
          </CModalFooter>
        </CModal>
      </>
    );
  }
}

export default EmailModal;
