import React, { Component } from "react";
import CIcon from "@coreui/icons-react";

class NoData extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    return (
      <div>
        <div className="text-center">
          <h3>
            NO DATA &nbsp;
            <CIcon name="cil-ban" className="noDataIcon" />
          </h3>
        </div>
      </div>
    );
  }
}

export default NoData;
