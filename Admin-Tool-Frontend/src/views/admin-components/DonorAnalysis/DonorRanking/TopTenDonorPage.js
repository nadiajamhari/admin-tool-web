import React, { Component } from "react";
import {
  CCard,
  CCardBody,
  CRow,
  CCol,
  CCardHeader,
  CDataTable,
  CInputCheckbox,
  CButton,
} from "@coreui/react";
import EmailModal from "./EmailModal";
import api from "../../../../services/api";
import Swal from "sweetalert2";

const fields = [
  "",
  "Position",
  "Name",
  "Email",
  "Recently",
  "Frequently",
  "Monetary",
  "AverageMonetary",
  "TotalMarks",
];

class TopTenDonorPage extends Component {
  constructor() {
    super();
    this.state = {
      type: "",
      ListOfDonors: [],
      emailModal: false,
      selectedList: [],
      masterChecked: false,
      content: "",
      filePath: null,
      filePaths: [],
      previewFile: [],
    };
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
    this.selectAll = this.selectAll.bind(this);
    this.handleContent = this.handleContent.bind(this);
    this.handleFile = this.handleFile.bind(this);
    this.deleteFile = this.deleteFile.bind(this);
  }

  componentDidMount() {
    this.getTopTenDonorList();
  }

  getTopTenDonorList() {
    api
      .get("/api/getTopTenDonorList/" + this.props.match.params.type)
      .then((response) => {
        this.setState({
          ListOfDonors: response.data.donorList,
        });
      })
      .catch((error) => {
        Swal.fire({
          title: "Error",
          text: error,
          icon: "error",
        });
      });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  selectAll(e) {
    let tempList = this.state.ListOfDonors;
    // Check/ UnCheck All Items
    tempList.map((donor) => (donor.selected = e.target.checked));
    //Update State
    this.setState({
      masterChecked: e.target.checked,
      ListOfDonors: tempList,
      selectedList: this.state.ListOfDonors.filter((e) => e.selected),
    });
  }

  selectChange(e, item) {
    let tempList = this.state.ListOfDonors;
    tempList.map((donor) => {
      if (donor.Id === item.Id) {
        donor.selected = e.target.checked;
      }
      return donor;
    });

    //To Control Master Checkbox State
    const totalItems = this.state.ListOfDonors.length;
    const totalCheckedItems = tempList.filter((e) => e.selected).length;

    // Update State
    this.setState({
      masterChecked: totalItems === totalCheckedItems,
      ListOfDonors: tempList,
      selectedList: this.state.ListOfDonors.filter((e) => e.selected),
    });
  }

  emailModalForm() {
    this.clearForm();
    this.setState({
      emailModal: !this.state.emailModal,
    });
  }

  handleContent(content) {
    this.setState(
      {
        content: content,
      },
      () => {
        console.log(this.state.content);
      }
    );
  }

  handleFile = (event) => {
    let file = event.target.files[0];
    const id = this.state.previewFile.length + 1;
    this.setState(
      (prevState) => ({
        filePaths: [
          ...prevState.filePaths,
          { id: "prevFile" + id, file: file },
        ],
        previewFile: [
          ...prevState.previewFile,
          { id: "prevFile" + id, file: file.name },
        ],
        filePath: null,
      }),
      () => {
        const fileInput = document.querySelector("#filePath");
        fileInput.value = "";
      }
    );
  };

  deleteFile(id) {
    var arrayPrev = this.state.previewFile; // make a separate copy of the array
    var arrayFile = this.state.filePaths; // make a separate copy of the array

    let deleteIndex = arrayPrev.findIndex((x) => x.id === id);
    if (deleteIndex > -1) {
      arrayPrev.splice(deleteIndex, 1);
      arrayFile.splice(deleteIndex, 1);
      this.setState({
        previewFile: arrayPrev,
        filePaths: arrayFile,
      });
    }
  }

  clearForm() {
    this.setState({
      content: "",
      filePaths: [],
      previewFile: [],
    });
  }

  sendEmail = () => {
    Swal.fire({
      title: "Loading...",
      showConfirmButton: false,
    });

    let formData = new FormData();
    formData.append("content", this.state.content);

    this.state.filePaths.forEach((file) => {
      formData.append("filePath[]", file.file);
    });

    this.state.selectedList.forEach((item) => {
      formData.append("email[]", item.Email);
    });

    this.state.selectedList.forEach((item) => {
      formData.append("name[]", item.Name);
    });
    formData.append("type", this.props.match.params.type);

    api
      .post("api/sendBroadcastEmail", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(() => {
        this.emailModalForm();
        Swal.fire({
          icon: "success",
          title: "Email Sent!",
          html: "An email has been sent",
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        }).then((result) => {
          if (result.isConfirmed) {
            this.getTopTenDonorList();
          }
        });
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          text: error.response.data.message,
          showConfirmButton: true,
          confirmButtonColor: "#5bc0de",
        });
      });
  };

  detailPage(id) {
    window.location.hash = "/admin/donor-analysis-details/" + id;
  }

  render() {
    return (
      <div>
        <CRow className="justify-content-center TopTenDonorPage">
          <CCol>
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm={8}>
                    <h3>List of Top 10 {this.props.match.params.type}</h3>
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      variant="outline"
                      color="dark"
                      disabled={this.state.selectedList.length === 0}
                      onClick={this.emailModalForm.bind(this)}
                    >
                      Send Email
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.ListOfDonors}
                  fields={fields}
                  itemsPerPage={10}
                  clickableRows
                  onRowClick={(item) => this.detailPage(item.Id)}
                  columnHeaderSlot={{
                    "": (
                      <CInputCheckbox
                        id="masterCheck"
                        name="masterCheck"
                        className="masterChecked"
                        onChange={this.selectAll}
                        checked={this.state.masterChecked}
                        style={{ marginLeft: "0rem" }}
                      />
                    ),
                  }}
                  scopedSlots={{
                    "": (item) => (
                      <td
                        onClick={this.disableOnRowClick}
                        className={item.selected ? "selected" : ""}
                      >
                        <CInputCheckbox
                          name={item.Id}
                          value={item.Id}
                          onChange={(e) => this.selectChange(e, item)}
                          checked={item.selected}
                          style={{ marginLeft: "0rem" }}
                        />
                      </td>
                    ),
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
        <EmailModal
          emailModal={this.state.emailModal}
          emailModalForm={this.emailModalForm.bind(this)}
          handleContent={this.handleContent}
          content={this.state.content}
          sendEmail={this.sendEmail.bind(this)}
          handleFile={this.handleFile}
          previewFile={this.state.previewFile}
          deleteFile={this.deleteFile}
        />
      </div>
    );
  }
}

export default TopTenDonorPage;
