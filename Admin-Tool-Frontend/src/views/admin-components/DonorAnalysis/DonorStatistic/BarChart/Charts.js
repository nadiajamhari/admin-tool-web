import React, { Component } from "react";
import { CChartBar } from "@coreui/react-chartjs";

class Charts extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const quantity = this.props.quantity;
    const labels = this.props.labels;
    const dataLabel = this.props.data;

    return (
      <CChartBar
        datasets={[
          {
            label: dataLabel,
            backgroundColor: "#228541",
            data: quantity,
          },
        ]}
        labels={labels}
        options={{
          tooltips: {
            enabled: true,
          },
        }}
      />
    );
  }
}

export default Charts;
