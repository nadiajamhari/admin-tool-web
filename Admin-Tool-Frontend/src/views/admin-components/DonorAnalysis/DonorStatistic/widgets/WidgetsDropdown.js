import React, { Component } from "react";
import { CWidgetDropdown, CRow, CCol } from "@coreui/react";

class WidgetsDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <CRow className="justify-content-center">
        <CCol sm="6" lg="5">
          <CWidgetDropdown
            className="taxCount"
            color={"gradient-success"}
            header={this.props.totalMoney}
            text={"Total Money"}
          ></CWidgetDropdown>
        </CCol>

        <CCol sm="6" lg="5">
          <CWidgetDropdown
            className="taxCount"
            color={"gradient-success"}
            header={this.props.totalTax.toString()}
            text={"Total Tax Exemption"}
          ></CWidgetDropdown>
        </CCol>
      </CRow>
    );
  }
}

export default WidgetsDropdown;
