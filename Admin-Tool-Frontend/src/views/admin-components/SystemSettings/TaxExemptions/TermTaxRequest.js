import React, { Component } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CDataTable,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CInput,
  CButton,
} from "@coreui/react";
import api from "../../../../services/api";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Swal from "sweetalert2";

class TermTaxRequest extends Component {
  constructor() {
    super();
    this.state = {
      terms: [],
      id: "",
      term: "",
      addModal: false,
      isEdit: false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    this.getAllTerms();
  }

  getAllTerms() {
    api.get("/api/getAllTerms").then((response) => {
      this.setState({
        terms: response.data.term,
      });
    });
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  resetForm() {
    this.setState({
      term: "",
      isEdit: false,
      id: "",
    });
  }

  addModal(isEdit, id, term) {
    this.resetForm();
    this.setState({
      addModal: !this.state.addModal,
    });

    if (isEdit === true || isEdit === "true") {
      this.setState({
        isEdit: true,
        term: term,
        id: id,
      });
    }
  }

  confirmAdd() {
    const data = {
      term: this.state.term,
    };
    api.post("/api/addTerm", data).then(() => {
      this.addModal();
      Swal.fire({
        icon: "success",
        title: "Term & Condition Added!",
        text: "Term & Condition added successfully",
        showConfirmButton: false,
        confirmButtonColor: "#225841",
        timer: 3000,
      });
      this.getAllTerms();
    });
  }

  updateData() {
    const data = {
      term: this.state.term,
    };

    api.put("api/updateTerm/" + this.state.id, data).then((response) => {
      this.addModal();
      Swal.fire({
        icon: "success",
        title: "Term & Condition updated!",
        text: "Term & Condition updated successfully",
        showConfirmButton: false,
        confirmButtonColor: "#225841",
        timer: 3000,
      });
      this.getAllTerms();
    });
  }

  delete(id, term) {
    Swal.fire({
      title: "Are you sure?",
      html: "You will lose these data: <br> <strong>Term:</strong> " + term,
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        api
          .delete("/api/deleteTerm/" + id)
          .then(() => {
            Swal.fire({
              title: "Term & Condition deleted!",
              text: "Term & Condition deleted successfully",
              icon: "success",
              showConfirmButton: false,
              timer: 3000,
            }).then(() => {
              this.getAllTerms();
            });
          })
          .catch((error) => {
            Swal.fire({
              title: "Error",
              text: error.response.data,
              icon: "error",
              confirmButtonColor: "#5bc0de",
            });
          });
      }
    });
  }

  render() {
    const { isEdit } = this.state;
    const fields = ["No", "Term", "Action"];
    return (
      <div>
        <CRow>
          <CCol>
            <CCard>
              <CCardHeader>
                <CRow>
                  <CCol sm={8}>
                    <h3>List of Tax Request Terms & Conditions</h3>{" "}
                  </CCol>
                  <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                    <CButton
                      color="dark"
                      variant="outline"
                      onClick={this.addModal.bind(this)}
                    >
                      <CIcon content={freeSet.cilPlus} />
                      Term
                    </CButton>
                  </CCol>
                </CRow>
              </CCardHeader>
              <CCardBody>
                <CDataTable
                  items={this.state.terms}
                  fields={fields}
                  bordered
                  itemsPerPage={10}
                  pagination
                  scopedSlots={{
                    Action: (item) => (
                      <td>
                        <CButton
                          color="info"
                          variant="outline"
                          onClick={this.addModal.bind(
                            this,
                            true,
                            item.Id,
                            item.Term
                          )}
                        >
                          <CIcon name="cil-pencil" />
                        </CButton>
                        &nbsp;
                        <CButton
                          color="danger"
                          variant="outline"
                          onClick={this.delete.bind(this, item.Id, item.Term)}
                        >
                          <CIcon name="cil-trash" />
                        </CButton>
                      </td>
                    ),
                  }}
                />
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>

        <CModal
          alignment="center"
          show={this.state.addModal}
          closeOnBackdrop={false}
          onClose={this.addModal.bind(this)}
        >
          <CModalHeader closeButton>
            <CModalTitle>
              {isEdit ? "Update Term & Condition" : "Add New Term & Condition"}
            </CModalTitle>
          </CModalHeader>
          <CModalBody>
            <div className="px-4">
              <CRow>Term:</CRow>
              <CRow className="py-1">
                <CInput
                  id="term"
                  name="term"
                  value={this.state.term}
                  onChange={this.handleChange}
                />
              </CRow>
            </div>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.addModal.bind(this)}>
              Cancel
            </CButton>
            <CButton
              color="success"
              onClick={
                isEdit ? this.updateData.bind(this) : this.confirmAdd.bind(this)
              }
            >
              {isEdit ? "Save" : "Add"}
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default TermTaxRequest;
