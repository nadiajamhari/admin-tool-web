import React, { Component } from "react";
import {
  CCol,
  CForm,
  CFormGroup,
  CInputFile,
  CInput,
  CLabel,
  CButton,
} from "@coreui/react";

class SendEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <CForm className="form-horizontal">
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="receiver">To</CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInput
                type="email"
                id="receiver"
                name="email"
                autoComplete="email"
                value={this.props.email}
                readOnly
              />
            </CCol>
          </CFormGroup>
          <CFormGroup row>
            <CCol md="3">
              <CLabel htmlFor="receipt">Receipt</CLabel>
            </CCol>
            <CCol xs="12" md="9">
              <CInputFile
                id="receipt"
                name="receipt"
                type="file"
                onChange={this.props.handleFile}
              />
            </CCol>
          </CFormGroup>
          <div className="float-right">
            <CButton
              type="reset"
              size="sm"
              color="danger"
              onClick={this.props.clearForm}
            >
              Cancel
            </CButton>{" "}
            <CButton
              size="sm"
              color="success"
              onClick={this.props.sendEmail}
              disabled={this.props.receipt === null}
            >
              Send
            </CButton>
          </div>
        </CForm>
      </div>
    );
  }
}

export default SendEmail;
