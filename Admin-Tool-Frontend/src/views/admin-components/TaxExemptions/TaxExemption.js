import React, { Component } from "react";
import { CCard } from "@coreui/react";
import Table from "./TaxExemptionTable";

class TaxExemption extends Component {
  render() {
    return (
      <div>
        <CCard>
          <Table />
        </CCard>
      </div>
    );
  }
}

export default TaxExemption;
