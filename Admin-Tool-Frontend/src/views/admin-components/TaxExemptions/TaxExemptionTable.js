import React, { Component } from "react";
import {
  CCardHeader,
  CCardBody,
  CDataTable,
  CBadge,
  CInputCheckbox,
  CRow,
  CCol,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CCollapse,
  CButton,
  CInputGroup,
  CInputGroupAppend,
  CLabel,
  CFormGroup,
  CInput,
  CForm,
} from "@coreui/react";
import api from "../../../services/api";
import dateFormat from "dateformat";
import swal from "sweetalert2";
import StatusOption from "./StatusOption";
import Loader from "src/containers/Loader";
import CIcon from "@coreui/icons-react";
import { RangeDatePicker } from "@y0c/react-datepicker";
import "@y0c/react-datepicker/assets/styles/calendar.scss";
import { startOfMonth } from "date-fns";

class TaxExemptionTable extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      statusId: 0,
      taxExemptions: [],
      page: [],
      colorLists: [],
      modalStatus: false,
      isLoading: false,
      selectedList: [],
      masterChecked: false,
      updateStatusModal: false,
      remark: "",
      accordion: false,
      startDate: startOfMonth(new Date()),
      endDate: new Date(),
      filter: "",
    };

    this.detailPage = this.detailPage.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.updateStatusModal = this.updateStatusModal.bind(this);
    this.updateStatus = this.updateStatus.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.selectAll = this.selectAll.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }
  componentDidMount() {
    this._isMounted = true;
    this.loadTaxExemption();
    this.getStatusColor();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  getStatusColor() {
    api.get("/api/statusColor").then((response) => {
      this.setState({
        colorLists: response.data,
      });
    });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleDate =
    () =>
    (...args) => {
      this.setState({
        startDate: args[0],
        endDate: args[1],
      });
    };

  selectAll(e) {
    let tempList = this.state.taxExemptions;
    // Check/ UnCheck All Items
    //get current active page
    var pageContainer = document.querySelector(".pagination");
    var childPage = pageContainer.querySelector(".active");
    var content = childPage.textContent;
    let firstArray = 0;
    let lastArray = 0;

    let pageTempList = this.state.page;

    for (let page of pageTempList) {
      if (page.page === parseInt(content)) {
        if (parseInt(content) > 1) {
          firstArray = 10 * (parseInt(content) - 1);
          lastArray = firstArray + page.itemCount;
        } else {
          lastArray = page.itemCount;
        }
        break;
      }
    }
    for (let i = firstArray; i < lastArray; i++) {
      tempList[i]["selected"] = e.target.checked;
    }

    //Update State
    this.setState(
      {
        masterChecked: e.target.checked,
        taxExemptions: tempList,
        selectedList: this.state.taxExemptions.filter((e) => e.selected),
      },
      () => {
        console.log(this.state.selectedList);
      }
    );
  }

  selectChange(e, item) {
    let tempList = this.state.taxExemptions;
    tempList.map((donor) => {
      if (donor.RequestID === item.RequestID) {
        donor.selected = e.target.checked;
      }
      return donor;
    });

    //To Control Master Checkbox State
    //get current active page
    var pageContainer = document.querySelector(".pagination");
    var childPage = pageContainer.querySelector(".active");
    var content = childPage.textContent;
    let totalItems = 0;

    let pageTempList = this.state.page;

    for (let page of pageTempList) {
      if (page.page === parseInt(content)) {
        totalItems = page.itemCount;
        break;
      }
    }

    const totalCheckedItems = tempList.filter((e) => e.selected).length;

    // Update State
    this.setState({
      masterChecked: totalItems === totalCheckedItems,
      taxExemptions: tempList,
      selectedList: this.state.taxExemptions.filter((e) => e.selected),
    });
  }

  resetForm() {
    this.setState({
      statusId: 0,
      remark: "",
    });
  }

  loadTaxExemption() {
    this.setState({
      isLoading: true,
    });
    api.get("/api/taxExemptions").then((response) => {
      this.setState({
        masterChecked: false,
        taxExemptions: response.data.taxList,
        page: response.data.page,
        isLoading: false,
      });
    });
  }

  updateStatusModal() {
    this.setState({
      updateStatusModal: !this.state.updateStatusModal,
    });
  }

  updateStatus() {
    const ids = JSON.stringify(this.state.selectedList);
    let formData = new FormData();
    formData.append("statusId", this.state.statusId);
    formData.append("remark", this.state.remark);
    formData.append("selected", ids);
    api
      .post("/api/updateStatusTaxs/", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then(() => {
        this.updateStatusModal();
        swal
          .fire({
            title: "Done!",
            text: "Status updated",
            icon: "success",
            timer: 2000,
            showConfirmButton: false,
          })
          .then(() => {
            this.setState({
              masterChecked: false,
              selectedList: [],
            });
            if (this.state.filter === "") {
              this.loadTaxExemption();
            } else {
              this.searchFilterTax();
            }

            this.resetForm();
          });
      });
  }

  detailPage(id) {
    window.location.hash = "/admin/tax-exemptions/tax-exemption-details/" + id;
  }

  exportExcel() {
    let result = new Date(this.state.endDate);
    result.setDate(result.getDate() + 1);

    const startDate = dateFormat(this.state.startDate, "yyyy-mm-dd");
    const endDate = dateFormat(result, "yyyy-mm-dd");

    api
      .get("/api/exportExcelTax/" + startDate + "/" + endDate)
      .then((response) => {
        window.open(response.data);
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.errors,
          icon: "error",
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        });
      });
  }

  exportPDF() {
    api
      .get("/api/exportPDFTax")
      .then((response) => {
        window.open(response.data);
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.errors,
          icon: "error",
          showConfirmButton: true,
          confirmButtonColor: "#225841",
        });
      });
  }

  setAccordion() {
    this.setState({
      accordion: !this.state.accordion,
    });
  }

  handleFilter = (e) => {
    console.log(e.target.value);
    this.setState(
      {
        filter: e.target.value,
      },
      () => {
        console.log("Filter : ", this.state.filter);
        if (this.state.filter === "") {
          this.loadTaxExemption();
        } else {
          this.searchFilterTax();
        }
      }
    );
  };

  searchFilterTax() {
    this.setState({
      isLoading: true,
    });
    const data = {
      filter: this.state.filter,
    };
    api.post("/api/filterTaxExemption", data).then((response) => {
      this.setState({
        masterChecked: false,
        taxExemptions: response.data.taxList,
        page: response.data.page,
        isLoading: false,
      });
    });
  }

  render() {
    const { isLoading, remark, updateStatusModal } = this.state;
    const fields = [
      "",
      "RequestID",
      "Name",
      "RequestDate",
      "PhoneNumber",
      "Amount",
      "Type",
      "Status",
    ];

    return (
      <>
        <CCardHeader>
          <CRow>
            <CCol sm={8}>
              <h3>List of Tax Exemption Request</h3>
            </CCol>
            <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
              <StatusOption
                handleChange={this.handleChange}
                pushArray={this.state.selectedList.length}
                remark={remark}
                updateStatusModal={updateStatusModal}
                updateModal={this.updateStatusModal}
                statusId={this.state.statusId}
                updateStatus={this.updateStatus}
              />
              &nbsp;
              <CDropdown className="btn-group">
                <CDropdownToggle variant="outline" color="dark">
                  Export
                </CDropdownToggle>
                <CDropdownMenu>
                  <CDropdownItem onClick={this.setAccordion.bind(this)}>
                    Excel
                  </CDropdownItem>
                  <CDropdownItem onClick={this.exportPDF}>PDF</CDropdownItem>
                </CDropdownMenu>
              </CDropdown>
            </CCol>
          </CRow>
          <CCollapse show={this.state.accordion === true}>
            <CCardBody>
              <CButton
                color="link"
                className="float-right"
                onClick={this.setAccordion.bind(this)}
              >
                <CIcon color="danger" name="cil-x" />
              </CButton>
              <CLabel>Please Select Date:</CLabel>
              <CInputGroup>
                <RangeDatePicker
                  initialStartDate={this.state.startDate}
                  initialEndDate={this.state.endDate}
                  onChange={this.handleDate()}
                />
                <CInputGroupAppend>
                  <CButton
                    type="button"
                    color="dark"
                    onClick={this.exportExcel.bind(this)}
                  >
                    Export
                  </CButton>
                </CInputGroupAppend>
              </CInputGroup>
            </CCardBody>
          </CCollapse>
        </CCardHeader>
        <CCardBody className="taxExemptionTable">
          {this.state.loadTaxExemption !== null && (
            <>
              <CForm inline>
                <CFormGroup className="pr-1 pb-2" inline>
                  <CLabel htmlFor="filter" className="pr-1">
                    Filter:
                  </CLabel>
                  <CInput
                    id="filter"
                    placeholder="Filter"
                    name="filter"
                    value={this.state.filter}
                    onChange={this.handleFilter}
                  />
                </CFormGroup>
              </CForm>

              {isLoading === false && (
                <>
                  <CDataTable
                    sorter
                    items={this.state.taxExemptions}
                    fields={fields}
                    bordered
                    loading={isLoading === true}
                    itemsPerPage={10}
                    pagination
                    hover
                    clickableRows
                    onRowClick={(item) => this.detailPage(item.RequestID)}
                    columnHeaderSlot={{
                      "": (
                        <CInputCheckbox
                          className="masterChecked"
                          id="masterChecked"
                          name="masterChecked"
                          disabled={this.state.taxExemptions == null}
                          onChange={this.selectAll}
                          checked={this.state.masterChecked}
                          style={{ marginLeft: "0rem" }}
                        />
                      ),
                    }}
                    scopedSlots={{
                      "": (item) => (
                        <td
                          onClick={this.disableOnRowClick}
                          className={item.selected ? "selected" : ""}
                        >
                          <CInputCheckbox
                            id={item.RequestID}
                            name={item.RequestID}
                            value={item.RequestID}
                            checked={item.selected}
                            onChange={(e) => this.selectChange(e, item)}
                            style={{ marginLeft: "0rem" }}
                          />
                        </td>
                      ),
                      Status: (item) => (
                        <td>
                          <CBadge color={item.ColorStatus}>
                            {item.Status}
                          </CBadge>
                        </td>
                      ),
                      RequestDate: (item) => (
                        <td>{dateFormat(item.RequestDate, "dd/mm/yyyy")}</td>
                      ),
                    }}
                  />
                </>
              )}
              {isLoading === true && <Loader />}
            </>
          )}
        </CCardBody>
      </>
    );
  }
}

export default TaxExemptionTable;
