import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";

class ModalNewEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <CModal
          alignment="center"
          show={
            this.props.isEdit
              ? this.props.modalEditEmail
              : this.props.modalAddEmail
          }
          onClose={
            this.props.isEdit
              ? this.props.modalEditEmailForm
              : this.props.modalAddEmailForm
          }
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>
              {this.props.isEdit ? "Edit Email" : "Add Email"}
            </CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="inputAddEmail">Email</CLabel>
                  <CInput
                    id="inputAddEmail"
                    name="inputAddEmail"
                    value={this.props.inputAddEmail}
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="addPassword">Password</CLabel>
                  <CInput
                    id="password"
                    name="inputAddPassword"
                    value={this.props.inputAddPassword}
                    onChange={this.props.handleChange}
                  />
                  {this.props.isEdit && (
                    <span className="required-text">
                      Do not fill the input if you want to remain old password
                    </span>
                  )}
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="secondary"
              onClick={
                this.props.isEdit
                  ? this.props.modalEditEmailForm
                  : this.props.modalAddEmailForm
              }
            >
              Cancel
            </CButton>
            <CButton
              color="success"
              onClick={
                this.props.isEdit ? this.props.editEmail : this.props.addEmail
              }
            >
              Add
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default ModalNewEmail;
