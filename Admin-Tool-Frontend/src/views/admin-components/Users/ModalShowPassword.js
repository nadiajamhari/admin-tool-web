import React, { Component } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CCol,
  CRow,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";

class ModalShowPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
      <div>
        <CModal
          alignment="center"
          show={this.props.modal}
          onClose={this.props.modalForm}
          closeOnBackdrop={false}
        >
          <CModalHeader closeButton>
            <CModalTitle>Show Password</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CRow>
              <CCol xs="12">
                <strong>
                  <p>
                    System is trying to show password. Type your password to
                    allow this
                  </p>
                </strong>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <p>Name: {this.props.userName}</p>
                <p>Email: {this.props.userEmail}</p>
              </CCol>
            </CRow>
            <CRow>
              <CCol xs="12">
                <CFormGroup>
                  <CLabel htmlFor="password">Password</CLabel>
                  <CInput
                    type="password"
                    name="password"
                    autoComplete="new-password"
                    onChange={this.props.handleChange}
                  />
                </CFormGroup>
              </CCol>
            </CRow>
          </CModalBody>
          <CModalFooter>
            <CButton color="secondary" onClick={this.props.modalForm}>
              Cancel
            </CButton>
            <CButton color="success" onClick={this.props.showPassword}>
              Show
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default ModalShowPassword;
