import React, { Component } from "react";
import {
  CRow,
  CCol,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
} from "@coreui/react";
import api from "src/services/api";
import Loader from "src/containers/Loader";

class AssetLog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usage_logs: [],
      items_ids: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    this.loadUsageLogs();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    this.setState({
      items_ids: nextProps.items_ids,
      usage_logs: nextProps.usage_logs,
      isLoading: false,
    });
  }

  loadUsageLogs() {
    this.setState({
      isLoading: true,
    });
    if (this.props.items_ids == null) {
      api.get("/api/getLogs").then((res) => {
        this.setState({
          usage_logs: res.data,
          isLoading: false,
        });
      });
    }
  }

  render() {
    const { isLoading } = this.state;
    let fields = [];
    let data = [];
    if (this.props.items_ids == null) {
      fields = [
        "StaffName",
        "Item",
        "Purpose",
        "QuantityTaken",
        "DateTaken",
        "DateReturned",
        // "QuantityReturned",
      ];
    } else {
      fields = [
        "StaffName",
        "ItemCode",
        "Purpose",
        "QuantityTaken",
        "DateTaken",
        "DateReturned",
        // "QuantityReturned",
      ];
    }
    if (this.state.usage_logs) {
      this.state.usage_logs.forEach((log) => {
        data.push({
          // LogID: log.id,
          StaffName: log.staff_name,
          Item: log.asset_item,
          ItemCode: log.item_code,
          Purpose: log.purpose,
          SingleUse: log.single_use,
          DateTaken: log.requested_at,
          QuantityTaken: log.requested_qty,
          DateReturned: log.returned_at,
          QuantityReturned: log.returned_qty,
        });
      });
    }

    // console.log(this.state.usage_logs);

    return (
      <div className="asset-table">
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>List of Asset Usage Logs</h3>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {isLoading === true && <Loader />}
            {isLoading === false && (
              <CDataTable
                items={data}
                fields={fields}
                itemsPerPage={5}
                pagination
                itemsPerPageSelect
                sorter
                hover
                // clickableRows
                // onRowClick={(item) => this.detailPage(item)}
                tableFilter
                scopedSlots={{
                  DateReturned: (item) => (
                    <td
                      style={
                        parseInt(item.SingleUse) === 1
                          ? { color: "none" }
                          : item.QuantityReturned === null
                          ? { color: "#db2127" }
                          : { color: "none" }
                      }
                    >
                      {parseInt(item.SingleUse) === 1
                        ? "-"
                        : item.QuantityReturned === null
                        ? item.DateReturned + " (Due)"
                        : item.DateReturned}
                    </td>
                  ),
                  QuantityReturned: (item) => (
                    <td
                      style={
                        parseInt(item.SingleUse) === 1
                          ? { color: "none" }
                          : item.QuantityReturned === null
                          ? { color: "#db2127" }
                          : { color: "none" }
                      }
                    >
                      {parseInt(item.SingleUse) === 1
                        ? "-"
                        : item.QuantityReturned === null
                        ? "0"
                        : item.QuantityReturned}
                    </td>
                  ),
                }}
              />
            )}
          </CCardBody>
        </CCard>{" "}
      </div>
    );
  }
}

export default AssetLog;
