import React, { Component } from "react";
import {
  CButton,
  CDataTable,
  CCard,
  CCardHeader,
  CCardBody,
  CRow,
  CCol,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CForm,
  CInput,
  CInputGroup,
  CInvalidFeedback,
  CLabel,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";
import Loader from "src/containers/Loader";
import api from "src/services/api";
import swal from "sweetalert2";

class Department extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showAddModal: false,
      isAddMode: true,
      department_id: "",
      department_name: "",
      errorList: [],
    };

    this.setModal = this.setModal.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);
    this.disableOnRowClick = this.disableOnRowClick.bind(this);
  }

  setModal(action, id, name) {
    this.setState((prevState) => ({ showAddModal: !prevState.showAddModal }));
    var modal = document.getElementById("deptModal");

    // Edit Asset Modal
    if (action === false) {
      modal.innerHTML = "Edit Department";
      this.setState({ isAddMode: false });
      this.setState({ department_id: id });
      this.setState({ department_name: name });
      this.setState({ errorList: [] });
    }

    // Add New Asset Modal
    else {
      modal.innerHTML = "Add Department";
      this.setState({ isAddMode: true });
      this.setState({ department_id: "" });
      this.setState({ department_name: "" });
      this.setState({ errorList: [] });
    }
  }

  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleAdd() {
    swal.fire({
      title: "Adding...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const data = { department: this.state.department_name };

    api
      .post("/api/addDepartment", data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              html: "Department <b>" + data.department + "</b> is added!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.props.loadDepartments();
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  handleDelete(id, name) {
    swal
      .fire({
        title: "Are you sure?",
        html:
          "You will lose every assets <br> in <b>" + name + "</b> department!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      })
      .then((result) => {
        if (result.isConfirmed) {
          swal.fire({
            title: "Deleting...",
            showConfirmButton: false,
            didOpen: () => {
              swal.showLoading();
            },
          });
          api
            .delete("/api/deleteDepartment/" + id)
            .then(() => {
              swal
                .fire({
                  html: "Department <b>" + name + "</b> deleted!",
                  icon: "success",
                  confirmButtonColor: "#5bc0de",
                })
                .then(() => {
                  this.props.loadDepartments();
                });
            })
            .catch((error) => {
              swal.fire({
                title: "Error",
                html: "Please delete related assets <br> to this department first.",
                icon: "error",
                confirmButtonColor: "#5bc0de",
              });
            });
        }
      });
  }

  handleUpdate() {
    swal.fire({
      title: "Updating...",
      showConfirmButton: false,
      didOpen: () => {
        swal.showLoading();
      },
    });

    const data = {
      department: this.state.department_name,
    };

    api
      .put("/api/updateDepartment/" + this.state.department_id, data)
      .then((res) => {
        swal.close();
        if (res.data.status === 200) {
          this.setModal();
          swal
            .fire({
              title: "Done!",
              html: "Department name updated successfully!",
              icon: "success",
              confirmButtonColor: "#5bc0de",
            })
            .then(() => {
              this.props.loadDepartments();
            });
        } else {
          this.setState({ errorList: res.data.errors });
        }
      })
      .catch((error) => {
        console.log(JSON.stringify(error.response.data.errors));
        swal.fire({
          title: "Error",
          text: error.response.data.message,
          icon: "error",
          confirmButtonColor: "#5bc0de",
        });
      });
  }

  disableOnRowClick(e) {
    e.stopPropagation();
  }

  render() {
    const fields = ["Department", "TotalCategories", "Action"];
    const list = [];

    if (this.props.departments) {
      this.props.departments.forEach((department) => {
        list.push({
          DepartmentID: department.id,
          Department: department.department,
          TotalCategories: department.asset_category.length,
          Categories: department.asset_category,
        });
      });
    }

    return (
      <div>
        <CCard>
          <CCardHeader>
            <CRow>
              <CCol sm={8}>
                <h3>List of Departments</h3>
              </CCol>
              <CCol className="d-grid gap-2 d-md-flex justify-content-md-end">
                <CButton color="dark" variant="outline" onClick={this.setModal}>
                  <CIcon content={freeSet.cilPlus} /> Department
                </CButton>
              </CCol>
            </CRow>
          </CCardHeader>
          <CCardBody>
            {this.props.isLoadingDept === true && <Loader />}
            {this.props.isLoadingDept === false && (
              <CDataTable
                fields={fields}
                items={list}
                sorter
                hover
                clickableRows
                onRowClick={(item) => this.props.setShowCategory(item)}
                scopedSlots={{
                  Action: (item) => (
                    <td onClick={this.disableOnRowClick}>
                      <CButton
                        color="info"
                        variant="outline"
                        onClick={this.setModal.bind(
                          this,
                          false,
                          item.DepartmentID,
                          item.Department
                        )}
                      >
                        <CIcon name="cil-pencil" />
                      </CButton>{" "}
                      &nbsp;
                      <CButton
                        color="danger"
                        variant="outline"
                        onClick={this.handleDelete.bind(
                          this,
                          item.DepartmentID,
                          item.Department
                        )}
                      >
                        <CIcon name="cil-trash" />
                      </CButton>
                    </td>
                  ),
                }}
              ></CDataTable>
            )}
          </CCardBody>
        </CCard>

        <CModal show={this.state.showAddModal} onClose={this.setModal}>
          <CModalHeader closeButton>
            <CModalTitle id="deptModal">Add Department</CModalTitle>
          </CModalHeader>
          <CModalBody>
            <CForm>
              <CInputGroup className="mb-4">
                <CInput
                  type="hidden"
                  name="department_id"
                  value={this.state.department_id}
                />
              </CInputGroup>
              <CInputGroup className="mb-4">
                <CCol sm="4" className="p-1">
                  <CLabel>Department Name</CLabel>
                </CCol>
                <CCol sm="8" className="p-0">
                  <CInput
                    type="text"
                    placeholder="Enter department name"
                    name="department_name"
                    value={this.state.department_name}
                    onChange={this.handleChange}
                    invalid={this.state.errorList["department"] ? true : false}
                  />
                  <CInvalidFeedback>
                    <span className="error-text">
                      {this.state.errorList["department"]}
                    </span>
                  </CInvalidFeedback>
                </CCol>
              </CInputGroup>
            </CForm>
          </CModalBody>
          <CModalFooter>
            <CButton
              color="success"
              onClick={
                this.state.isAddMode ? this.handleAdd : this.handleUpdate
              }
            >
              {this.state.isAddMode ? "Add" : "Save"}
            </CButton>{" "}
            <CButton color="secondary" onClick={this.setModal.bind(this)}>
              Cancel
            </CButton>
          </CModalFooter>
        </CModal>
      </div>
    );
  }
}

export default Department;
