import React, { Component } from "react";
import { CRow, CCol } from "@coreui/react";
import Department from "./Department";
import Category from "./Category";
import api from "src/services/api";

class ManagePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      departmentId: "",
      departmentName: "",
      departments: [],
      categories: [],
      isLoadingDept: true,
      isShowCategory: false,
    };

    this.loadDepartments = this.loadDepartments.bind(this);
    this.setShowCategory = this.setShowCategory.bind(this);
  }

  componentDidMount() {
    this.loadDepartments();
  }

  loadDepartments() {
    this.setState({
      isLoadingDept: true,
    });
    api.get("/api/getDepartment").then((res) => {
      this.setState({
        departments: res.data,
        isLoadingDept: false,
        isShowCategory: false,
      });
    });
  }

  setShowCategory(item) {
    if (item != null) {
      if (
        this.state.departmentId === "" ||
        this.state.departmentId === item.DepartmentID
      ) {
        this.setState((prevState) => ({
          isShowCategory: !prevState.isShowCategory,
        }));
      }
      this.setState({
        departmentId: item.DepartmentID,
        departmentName: item.Department,
        categories: item.Categories,
      });
    }
  }

  render() {
    return (
      <div>
        <CRow>
          <CCol>
            <Department
              departments={this.state.departments}
              isLoadingDept={this.state.isLoadingDept}
              loadDepartments={this.loadDepartments}
              setShowCategory={this.setShowCategory}
            />
          </CCol>
          {this.state.isShowCategory && (
            <CCol>
              <Category
                departments={this.state.departments}
                departmentId={this.state.departmentId}
                departmentName={this.state.departmentName}
                categories={this.state.categories}
                loadDepartments={this.loadDepartments}
              />
            </CCol>
          )}
        </CRow>
      </div>
    );
  }
}

export default ManagePage;
