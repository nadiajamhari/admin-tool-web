import React, { Component } from "react";
import Table from "./AssetTable";
import Logs from "./AssetLog";

class ViewListAssets extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <Table />
        <Logs />
      </div>
    );
  }
}

export default ViewListAssets;
