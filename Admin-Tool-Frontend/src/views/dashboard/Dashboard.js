import React, { lazy, Component } from "react";
import Action from "../admin-components/organisationAssets/AssetLogAction";

// const WidgetsDropdown = lazy(() =>
//   import("../layout-component/widgets/WidgetsDropdown.js")
// );

const WidgetsDropdown = lazy(() => import("./widgets/WidgetsDropdown"));

class Dashboard extends Component {
  constructor() {
    super();
    this.state = {
      isRefresh: localStorage.getItem("refreshAfterLogin"),
    };
  }
  componentDidMount() {
    localStorage.getItem("token");
    this.loadPage();
  }

  loadPage() {
    if (this.state.isRefresh === "false") {
      window.location.reload();
      localStorage.setItem("refreshAfterLogin", true);
    }
  }
  render() {
    return (
      <>
        <WidgetsDropdown />
        <Action />
      </>
    );
  }
}

export default Dashboard;
