import React, { Component } from "react";
import { CWidgetDropdown, CRow, CCol } from "@coreui/react";
import api from "../../../services/api";

class WidgetsDropdown extends Component {
  constructor() {
    super();
    this.state = {
      status: [],
    };
  }
  componentDidMount() {
    this.loadStatus();
  }

  loadStatus() {
    api.get("/api/loadStatus").then((response) => {
      this.setState({
        status: response.data.status,
      });
    });
  }
  render() {
    return (
      <CRow>
        {this.state.status.map((data) => (
          <>
            <CCol sm="6" lg="3" key={data.status}>
              <CWidgetDropdown
                className="taxCount"
                color={"gradient-" + data.color}
                header={data.count.toString()}
                text={data.status}
              ></CWidgetDropdown>
            </CCol>
          </>
        ))}
      </CRow>
    );
  }
}

export default WidgetsDropdown;
