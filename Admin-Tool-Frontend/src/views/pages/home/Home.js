import React from "react";
import { CCol, CContainer, CRow, CButton } from "@coreui/react";

const Home = () => {
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="6">
            <div className="clearfix">
              <h4 className="pt-3">Future Landing Page</h4>
              <h3>here is the button choose yours</h3>
              <CRow className="align-items-center">
                <CCol col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
                  <CButton block color="primary" to="/admin">
                    Admin
                  </CButton>
                </CCol>
                <CCol col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
                  <CButton block color="primary" to="/tax">
                    tax
                  </CButton>
                </CCol>
                <CCol col="6" sm="4" md="2" xl className="mb-3 mb-xl-0">
                  <CButton block color="primary" to="/staff">
                    Staff
                  </CButton>
                </CCol>
              </CRow>{" "}
            </div>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Home;
