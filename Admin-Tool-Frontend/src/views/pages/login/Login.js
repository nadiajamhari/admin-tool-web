import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import api from "../../../services/api";
import Swal from "sweetalert2";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupAppend,
  CInputGroupPrepend,
  CInputGroupText,
  CInvalidFeedback,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { AiOutlineEye, AiOutlineEyeInvisible } from "react-icons/ai";

const iconOpen = <AiOutlineEye size={18} />;
const iconClose = <AiOutlineEyeInvisible size={18} />;

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      toHome: null,
      showAlert: false,
      showPassword: false,
      errorList: [],
    };
  }

  componentDidMount() {
    // console.log(window.history.length > 2);
    // if (window.history.length > 2) {
    //   this.setState({ showAlert: true });
    // }
  }

  handleEmail(e) {
    this.setState({ email: e.target.value });
  }

  handlePassword(e) {
    this.setState({ password: e.target.value });
  }

  togglePassword(e) {
    this.setState({ showPassword: !this.state.showPassword });
    e.preventDefault();
  }

  handleSubmit() {
    Swal.fire({
      title: "Logging in",
      showConfirmButton: false,
      didOpen: () => {
        Swal.showLoading();
      },
    });
    api.get("/sanctum/csrf-cookie").then(() => {
      const creds = {
        email: this.state.email,
        password: this.state.password,
      };
      api
        .post("/api/login", creds)
        .then((res) => {
          if (res.data.status === 200) {
            this.props.login(res.data.token);
            localStorage.setItem("token", res.data.token);
            localStorage.setItem("refreshAfterLogin", false);
            localStorage.setItem("users", JSON.stringify(res.data.user));
            this.setState({ toHome: "/admin/dashboard" });
          } else {
            Swal.close();
            this.setState({ errorList: res.data.errors });
          }
        })
        .catch((error) => {
          Swal.close();
          alert(error);
        });
    });
  }

  render() {
    if (this.state.toHome) {
      return <Redirect to={this.state.toHome} />;
    }
    return (
      <div className="c-app c-default-layout flex-row align-items-center">
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="5">
              {this.state.showAlert && (
                <div className="alert alert-warning">
                  {this.props.location.state.reason}
                </div>
              )}
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    <CForm>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <CInputGroup className="mb-3">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-user" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type="text"
                          placeholder="Email"
                          autoComplete="email"
                          onChange={this.handleEmail.bind(this)}
                          invalid={this.state.errorList["email"] ? true : false}
                        />
                        <CInvalidFeedback>
                          <span className="error-text">
                            {this.state.errorList["email"]}
                          </span>
                        </CInvalidFeedback>
                      </CInputGroup>
                      <CInputGroup className="mb-4">
                        <CInputGroupPrepend>
                          <CInputGroupText>
                            <CIcon name="cil-lock-locked" />
                          </CInputGroupText>
                        </CInputGroupPrepend>
                        <CInput
                          type={this.state.showPassword ? "text" : "password"}
                          placeholder="Password"
                          autoComplete="current-password"
                          onChange={this.handlePassword.bind(this)}
                          invalid={
                            this.state.errorList["password"] ? true : false
                          }
                        />
                        <CInputGroupAppend>
                          <CButton
                            color="light"
                            onClick={this.togglePassword.bind(this)}
                          >
                            {this.state.showPassword === true && (
                              <div>{iconOpen}</div>
                            )}
                            {this.state.showPassword === false && (
                              <div>{iconClose}</div>
                            )}
                          </CButton>
                        </CInputGroupAppend>
                        <CInvalidFeedback>
                          <span className="error-text">
                            {this.state.errorList["password"]}
                          </span>
                        </CInvalidFeedback>
                      </CInputGroup>
                      <CRow>
                        <CCol xs="6">
                          <CButton
                            type="button"
                            color="primary"
                            className="px-4"
                            onClick={this.handleSubmit.bind(this)}
                          >
                            Login
                          </CButton>
                        </CCol>
                        <CCol xs="6" className="text-right">
                          <CButton
                            color="link"
                            className="px-0"
                            to="/forgot-password"
                          >
                            Forgot password?
                          </CButton>
                        </CCol>
                      </CRow>
                    </CForm>
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      </div>
    );
  }
}

export default Login;
