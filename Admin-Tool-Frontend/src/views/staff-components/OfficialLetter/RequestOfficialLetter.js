import React, { Component } from "react";
import {
  //   CForm,
  //   CButton,
  CRow,
  CCol,
  CFormGroup,
  CInput,
  CLabel,
  CSelect,
  //   CModal,
  //   CModalHeader,
  //   CModalBody,
  //   CBadge,
  //   CDataTable,
  //   CInputCheckbox,
  //   CFormText,
  //   CInvalidFeedback,
} from "@coreui/react";
import DatePicker from "react-date-picker";
import api from "src/services/api";
import Content from "../SunEditorContent/SunEditor";
// import Swal from "sweetalert2";
// import CIcon from "@coreui/icons-react";

class RequestOfficialLetter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      types: [],
      typeId: 0,
      startDate: new Date(Date.now() + 3600 * 1000 * 24),
      date: new Date(Date.now() + 3600 * 1000 * 24),
      receiverName: "",
      states: [],
      street: "",
      city: "",
      stateId: 0,
      postalCode: "",
      content: "",
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleDate = this.handleDate.bind(this);
    this.handleContent = this.handleContent.bind(this);
  }

  componentDidMount() {
    this.getOfficialLettersType();
    this.getAllState();
  }

  getOfficialLettersType() {
    api.get("/api/getOfficialLettersType").then((response) => {
      this.setState({
        types: response.data.types,
      });
    });
  }

  getAllState() {
    api.get("/api/allState").then((response) => {
      this.setState({
        states: response.data,
      });
    });
  }

  handleDate(date) {
    this.setState({
      date: date,
    });
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleContent(content) {
    this.setState({
      content: content,
    });
  }

  render() {
    const {
      receiverName,
      startDate,
      date,
      typeId,
      street,
      city,
      stateId,
      postalCode,
    } = this.state;
    return (
      <CRow className="justify-content-center">
        <CCol xl={6}>
          <h4>Request Official Letter</h4>
          <hr />
          <CFormGroup>
            <CLabel htmlFor="type">
              Type <span className="required-text">*</span>
            </CLabel>
            <CSelect
              custom
              name="typeId"
              id={typeId}
              value={typeId}
              onChange={this.handleChange}
            >
              <option key="0" value="0" disabled>
                Please select
              </option>
              {this.state.types.map((type) => (
                <option value={type.id} key={type.id.toString() + type.type}>
                  {type.type}
                </option>
              ))}
            </CSelect>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="date-input">
              Date Need Letter <span className="required-text">*</span>
            </CLabel>
            <br></br>
            <DatePicker
              onChange={this.handleDate}
              name="date"
              value={date}
              minDate={startDate}
              format="dd-MM-y"
            />
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="receiverName">Receiver Name:</CLabel>
            <br></br>
            <CInput
              id="receiverName"
              name="receiverName"
              placeholder="Enter receiver name"
              value={receiverName}
              onChange={this.handleChange}
            />
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="address">
              Street <span className="required-text">*</span>
            </CLabel>
            <CInput
              id="street"
              placeholder="Street"
              name="street"
              value={street}
              onChange={this.handleChange}
            />
          </CFormGroup>
          <CRow className="my-0">
            <CCol sm="4">
              <CFormGroup>
                <CLabel htmlFor="city">
                  City <span className="required-text">*</span>
                </CLabel>
                <CInput
                  id="city"
                  name="city"
                  placeholder="City"
                  value={city}
                  onChange={this.handleChange}
                />
              </CFormGroup>
            </CCol>
            <CCol sm="4">
              <CFormGroup>
                <CLabel htmlFor="stateId">
                  State <span className="required-text">*</span>
                </CLabel>
                <CSelect
                  name="stateId"
                  id={stateId}
                  value={stateId}
                  onChange={this.handleChange}
                >
                  <option key="0" value="0" disabled>
                    Please select
                  </option>
                  {this.state.states.map((state) => (
                    <option value={state.id} key={state.id.toString()}>
                      {state.state}
                    </option>
                  ))}
                </CSelect>
              </CFormGroup>
            </CCol>
            <CCol sm="4">
              <CFormGroup>
                <CLabel htmlFor="postalCode">
                  Postal Code <span className="required-text">*</span>
                </CLabel>
                <CInput
                  id="postalCode"
                  placeholder="Postal Code"
                  name="postalCode"
                  value={postalCode}
                  onChange={this.handleChange}
                  required
                />
              </CFormGroup>
            </CCol>
          </CRow>
          <CFormGroup>
            <CLabel htmlFor="receiverName">Content:</CLabel>
            <br></br>
            <Content handleContent={this.handleContent} />
          </CFormGroup>
        </CCol>
      </CRow>
    );
  }
}

export default RequestOfficialLetter;
