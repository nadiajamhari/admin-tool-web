import React, { Component } from "react";
import {
  CRow,
  CCol,
} from "@coreui/react";
import RequestForm from "./RequestForm";

class RequestPage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <CRow className="justify-content-center">
          <CCol xl={6}>
            <RequestForm />
          </CCol>
        </CRow>
      </div>
    );
  }
}

export default RequestPage;
