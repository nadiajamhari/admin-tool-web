import {
  CButton,
  CModal,
  CModalHeader,
  CModalBody,
  CRow,
  CDataTable,
  CInputCheckbox,
  CModalTitle,
  CFormText,
  CFormGroup,
  CLabel,
  CInputGroup,
  CInput,
  CInputGroupAppend,
} from "@coreui/react";
import React, { Component } from "react";
import Loader from "src/containers/Loader";
import api from "src/services/api";
import swal from "sweetalert2";
import QrReader from "react-qr-reader";
import CIcon from "@coreui/icons-react";
import { freeSet } from "@coreui/icons";

class ReturnForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isShowModal: false,
      reader: false,
      codeInput: false,
      itemCode: "",
      itemLogs: [],
      checkedItems: new Map(),
      selectedList: [],
      dataToReturn: [],

      returnItemsValid: false,
      returnFormValid: false,
      errors: {},
    };

    this.setModal = this.setModal.bind(this);
    this.setQrReader = this.setQrReader.bind(this);
    this.setCodeInput = this.setCodeInput.bind(this);
    this.handleReadQr = this.handleReadQr.bind(this);
    this.handleErrorQr = this.handleErrorQr.bind(this);
    this.getItemLog = this.getItemLog.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.addSelected = this.addSelected.bind(this);
    this.handleReturn = this.handleReturn.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {}

  setModal(isClose) {
    this.setState({
      isShowModal: !this.state.isShowModal,
    });
    if (isClose) {
      this.setState({
        codeInput: false,
        reader: false,
      });
    }
  }

  setQrReader() {
    this.setState({ reader: true });
    this.setModal(false);
  }

  setCodeInput() {
    this.setState({ codeInput: true });
    this.setModal(false);
  }

  handleReadQr(result) {
    if (result) {
      let res = JSON.parse(result);
      this.setState({ itemCode: res.full_code });
      this.getItemLog();
    } else {
      console.log("Not found");
    }
  }

  handleErrorQr(error) {
    console.log(error);
  }

  getItemLog() {
    this.setState({
      isLoading: true,
    });
    api.get("/api/getLog/" + this.state.itemCode).then((res) => {
      this.setState({
        itemLogs: res.data,
        isLoading: false,
      });
    });
  }

  handleSelect(e) {
    const item = e.target.name;
    const isChecked = e.target.checked;
    this.setState((prevState) => ({
      checkedItems: prevState.checkedItems.set(item, isChecked),
    }));
  }

  addSelected() {
    let idArr = [];
    this.state.checkedItems.forEach((value, key) => {
      if (value === true) {
        idArr.push(key);
      }
    });
    let errors = {};
    if (idArr.length <= 0) {
      errors["selectedItems"] = "Please select at least one items";
      this.setState({ errors });
    } else {
      this.setState({ errors });
      idArr.forEach((id) => {
        this.state.itemLogs.forEach((element) => {
          if (element.log_id === Number(id)) {
            this.state.dataToReturn.push(element.log_id);
            this.state.selectedList.push(element);
          }
        });
      });
      this.validateField();
      this.setModal(true);
    }
  }

  handleReturn(e) {
    e.preventDefault();

    this.validateField();
    if (this.state.returnFormValid) {
      var data = {
        log_ids: this.state.dataToReturn,
      };

      api
        .put("/api/logReturn", data)
        .then((res) => {
          if (res.data.status === 200) {
            swal
              .fire({
                title: "Thank you!",
                text: "You have successfully returned used item(s).",
                icon: "success",
                confirmButtonColor: "#5bc0de",
              })
              .then(() => {
                window.location.reload();
              });
          }
        })
        .catch((error) => {
          swal.fire({
            title: "Error",
            text: error.message,
            icon: "error",
          });
        });
    }
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  validateField() {
    let returnItemsValid = this.state.returnItemsValid;
    let errors = {};
    if (this.state.selectedList.length <= 0) {
      errors["returnTable"] = "Please add items.";
    } else {
      returnItemsValid = true;
    }
    this.setState(
      {
        errors: errors,
        returnItemsValid: returnItemsValid,
      },
      () => {
        this.validateForm();
      }
    );
  }

  validateForm() {
    this.setState({
      returnFormValid: this.state.returnItemsValid,
    });
    return this.state.returnFormValid;
  }

  render() {
    const {
      isLoading,
      isShowModal,
      reader,
      codeInput,
      itemCode,
      itemLogs,
      selectedList,
    } = this.state;

    const items = selectedList;
    const fields = [
      { key: "item_code" },
      { key: "purpose" },
      { key: "requested_at" },
      // { key: "requested_qty" },
      // { key: "returned_qty" },
    ];

    return (
      <div className="container">
        <div className="justify-content-center">
          <h4>Return Form</h4>
          <hr />
          <div className="mt-4">
            <div style={{ paddingBottom: 10 }}>
              <CRow className="justify-content-center">
                <CButton color="dark" onClick={this.setQrReader}>
                  <CIcon content={freeSet.cilQrCode} size="2xl" /> Scan Qr Code
                </CButton>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <CButton color="dark" onClick={this.setCodeInput}>
                  <CIcon content={freeSet.cilKeyboard} size="2xl" /> Enter Code
                </CButton>
              </CRow>
            </div>
            {selectedList.length <= 0 && (
              <div>
                <p className="justify-content-center">
                  Please scan item QR Code to start.
                </p>
                <CFormText>
                  <span className="error-text">
                    {this.state.errors["returnTable"]}
                  </span>
                </CFormText>
              </div>
            )}
            {selectedList.length > 0 && (
              <div>
                <CDataTable
                  items={items}
                  fields={fields}
                  // loading={isLoading === true}
                />
              </div>
            )}

            <CButton
              className="float-right"
              color="success"
              // disabled={selectedList.length > 0 ? false : true} // invalid feedback, selectedItem must not be emtpty
              onClick={this.handleReturn}
            >
              Submit
            </CButton>
          </div>
        </div>
        <CModal
          show={isShowModal}
          onClose={this.setModal.bind(this, true)}
          closeOnBackdrop={false}
          className=""
        >
          <CModalHeader closeButton>
            <CModalTitle>{itemCode}</CModalTitle>
          </CModalHeader>
          <CModalBody>
            {reader && (
              <div>
                <CRow>
                  <QrReader
                    delay={300}
                    style={{ width: "100%" }}
                    onError={this.handleErrorQr}
                    onScan={this.handleReadQr}
                  />
                </CRow>
              </div>
            )}
            {codeInput && (
              <CFormGroup>
                <CLabel htmlFor="code-input">Item Code</CLabel>
                <CInputGroup>
                  <CInput
                    type="text"
                    id="code-input"
                    name="itemCode"
                    value={this.state.itemCode}
                    placeholder="Enter Item Code"
                    onChange={this.handleChange}
                  />
                  <CInputGroupAppend>
                    <CButton
                      variant="outline"
                      color="secondary"
                      onClick={this.getItemLog}
                    >
                      <CIcon content={freeSet.cilSearch} />
                    </CButton>
                  </CInputGroupAppend>
                </CInputGroup>
              </CFormGroup>
            )}
            {isLoading === true && itemCode && <Loader />}
            {isLoading === false && itemCode && (
              <div>
                <CDataTable
                  items={itemLogs}
                  fields={[
                    { key: " " },
                    { key: "purpose" },
                    { key: "requested_at" },
                  ]}
                  scopedSlots={{
                    " ": (item) => (
                      <td>
                        <CInputCheckbox
                          id="inline-checkbox1"
                          name={item.log_id}
                          value={item.log_id}
                          style={{
                            //   position: "absolute",
                            //   left: "50%",
                            //   top: "50%",
                            //   transform: "translate(-50%, -50%)",.bind(this,item)
                            marginLeft: "0rem",
                          }}
                          onChange={this.handleSelect}
                        />
                      </td>
                    ),
                  }}
                />
                <CFormText>
                  <span className="error-text">
                    {this.state.errors["selectedItems"]}
                  </span>
                </CFormText>
                <CButton
                  className="float-right m-2"
                  color="success"
                  onClick={this.addSelected.bind(this)}
                  // disabled={.length > 0 ? false : true} // invalid feedback, need to select an item
                >
                  Add
                </CButton>
              </div>
            )}
          </CModalBody>
        </CModal>
      </div>
    );
  }
}

export default ReturnForm;
